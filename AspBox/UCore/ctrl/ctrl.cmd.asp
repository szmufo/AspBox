<%
'######################################################################
'## ctrl.cmd.asp
'## -------------------------------------------------------------------
'## Feature     :   AspBox Mvc Cmd-Ctrl Block
'## Version     :   v1.0
'## Author      :   Lajox(lajox@19www.com)
'## Update Date :   2012/06/10 21:20
'## Description :   AspBox Mvc Cmd Control Block(CMD控制模块)
'######################################################################

Class Cls_Ctrl_CMD

	Private Sub Class_Initialize()
		AB.Use "db"
	End Sub

	Private Sub Class_Terminate()

	End Sub

	'@ *****************************************************************************
	'@ 过程名:  Ctrl.Cmd.RunSQL(s)
	'@ 返  回:  Recordset (Rs记录集)
	'@ 作  用:  执行SQL语句(并返回记录集)
	'==Param========================================================================
	'@ 参数 s 	: 字符串 [String] 执行的SQL语句
	'==DEMO=========================================================================
	'@ ab.use "mvc" : ctrl.use "cmd"
	'@ 'Ctrl.Cmd.RunSQL("DELETE FORM [table] WHERE id<2")
	'@ Dim Rs : Set Rs=Ctrl.Cmd.RunSQL("SELECT id FROM [table] WHERE id<2")
	'@ If Not Rs.Eof Then AB.C.PrintCn Rs("id")
	'@ *****************************************************************************

	Public Function RunSQL(Byval sql)
		On Error Resume Next
		sql = Trim(sql)
		If Lcase(Left(sql,6)) = "select" Then
			Set RunSQL = AB.db.ExeC(sql)
		Else
			AB.db.ExeC(sql)
		End If
		On Error Goto 0
	End Function

End Class
%>