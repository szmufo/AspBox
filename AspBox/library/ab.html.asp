<%
'######################################################################
'## ab.html.asp
'## -------------------------------------------------------------------
'## Feature     :   AspBox Html Class
'## Version     :   v1.0
'## Author      :   Lajox(lajox@19www.com)
'## Update Date :   2012/07/23 18:32
'## Description :   AspBox Html控件生成模块
'######################################################################

Class Cls_AB_Html

	Private Sub Class_Initialize

	End Sub

	Private Sub Class_Terminate

	End Sub

	'返回锚标签
	Public Function BackAnchor()
	    BackAnchor="<a href=""#"" onclick=""history.back()"">返回</a>"
	End Function

	'***************************************************
	'@ ab.html.anchor(text, src, args)
	'@ html之超链接控件
	'@ author:lajox; version:1.0.0 (2011-10-06)
	'==DEMO=============================================
	'@ ab.c.print ab.html.anchor("百度", "http://www.baidu.com/", "target=""_blank""")
	'***************************************************
	Public Function Anchor(Byval text, Byval src, Byval args)
		Dim temp:temp=""
		Dim aLink:aLink=""
		If Not IsNull(src) and Trim(src)<>"" Then aLink = " href=""" & src & """"
        temp = temp & "<a"& aLink & GetUpArgs("", args, 1) & ">" & text & "</a>"
		Anchor = temp
	End Function

	'***************************************************
	'@ ab.html.img(src, alt, args)
	'@ html之IMG标签
	'@ author:lajox; version:1.0.0 (2011-10-06)
	'==DEMO=============================================
	'@ ab.c.print ab.html.img("http://www.baidu.com/images/logo.gif", "百度Logo", "id='baidu_logo'")
	'***************************************************
	Public Function Img(Byval src, Byval alt, Byval args)
		Dim temp:temp=""
		Dim s_alt:s_alt=""
		If Not IsNull(alt) Or alt<>"" Then s_alt = " alt=""" & alt & """"
	    temp = "<img src=""" & src & """" & s_alt & GetUpArgs("", args, 1) & " />"
		Img = temp
	End Function

	'***************************************************
	'@ ab.html.label(id, text, args)
	'@ html之Label控件
	'@ author:lajox; version:1.0.0 (2011-10-06)
	'==DEMO=============================================
	'@ ab.c.print ab.html.label("", "这是个Label", "")
	'***************************************************
	Public Function Label(Byval id, Byval text, Byval args)
		Dim temp:temp=""
        temp = temp & "<label" & GetUpArgs(id, args, 1) & ">" & text & "</label>"
		Label = temp
	End Function

	'***************************************************
	'@ ab.html.li(id, text, args)
	'@ html之LI控件(组织Li节点)
	'@ author:lajox; version:1.0.0 (2011-10-06)
	'==DEMO=============================================
	'@ ab.c.print ab.html.li("menu", "菜单01", "style{list-style:none}")
	'***************************************************
	Public Function LI(Byval id, Byval text, Byval args)
		Dim temp:temp=""
        temp = temp & "<li" & GetUpArgs(id, args, 1) & ">" & text & "</li>"
		LI = temp
	End Function

	'***************************************************
	'@ ab.html.ul(id, text, args)
	'@ html之UL控件
	'@ author:lajox; version:1.0.0 (2011-10-06)
	'==DEMO=============================================
	'@ ab.html.ul("menu", "内容", "")
	'@ dim s_html_li : s_html_li = ""
	'@ s_html_li = s_html_li & ab.html.li("", "菜单1", "")
	'@ s_html_li = s_html_li & ab.html.li("", "菜单2", "")
	'@ ab.c.print ab.html.ul("menu", s_html_li, "")
	'***************************************************
	Public Function UL(Byval id, Byval text, Byval args)
		Dim temp:temp=""
        temp = temp & "<ul" & GetUpArgs(id, args, 1) & ">" & text & "</ul>"
		UL = temp
	End Function

	'***************************************************
	'@ ab.html.div(id, text, args)
	'@ html之DIV控件
	'@ author:lajox; version:1.0.0 (2011-10-06)
	'==DEMO=============================================
	'@ ab.c.print ab.html.div("block", "这是一个块", "")
	'***************************************************
	Public Function DIV(Byval id, Byval text, Byval args)
		Dim temp:temp=""
        temp = temp & "<div" & GetUpArgs(id, args, 1) & ">" & text & "</div>"
		DIV = temp
	End Function

	'***************************************************
	'@ ab.html.span(id, text, args)
	'@ html之SPAN控件
	'@ author:lajox; version:1.0.0 (2011-10-06)
	'==DEMO=============================================
	'@ ab.c.print ab.html.span("span", "这是个区域", "")
	'***************************************************
	Public Function SPAN(Byval id, Byval text, Byval args)
		Dim temp:temp=""
        temp = temp & "<span" & GetUpArgs(id, args, 1) & ">" & text & "</span>"
		SPAN = temp
	End Function

	'***************************************************
	'@ ab.html.p(id, text, args)
	'@ html之P段落标记
	'@ author:lajox; version:1.0.0 (2011-10-07)
	'==DEMO=============================================
	'@ ab.c.print ab.html.p("paragraph", "这是一个段落", "")
	'***************************************************
	Public Function P(Byval id, Byval text, Byval args)
		Dim temp:temp=""
        temp = temp & "<p" & GetUpArgs(id, args, 1) & ">" & text & "</p>"
		P = temp
	End Function

	'***************************************************
	'@ ab.html.pre(id, text, args)
	'@ html之pre标记
	'@ author:lajox; version:1.0.0 (2011-10-07)
	'==DEMO=============================================
	'@ ab.c.print ab.html.pre("paragraph", "这是一个段落", "")
	'***************************************************
	Public Function Pre(Byval id, Byval text, Byval args)
		Dim temp:temp=""
        temp = temp & "<pre" & GetUpArgs(id, args, 1) & ">" & text & "</pre>"
		Pre = temp
	End Function

	'***************************************************
	'@ ab.html.strong(id, text, args)
	'@ html之strong标记
	'@ author:lajox; version:1.0.0 (2011-10-07)
	'==DEMO=============================================
	'@ ab.c.print ab.html.strong("", "这里的字体突出", "")
	'***************************************************
	Public Function Strong(Byval id, Byval text, Byval args)
		Dim temp:temp=""
        temp = temp & "<strong" & GetUpArgs(id, args, 1) & ">" & text & "</strong>"
		Strong = temp
	End Function

	'***************************************************
	'@ ab.html.h(n, id, text, args)
	'@ html之h1~h6标记
	'@ author:lajox; version:1.0.0 (2011-10-07)
	'==DEMO=============================================
	'@ ab.c.print ab.html.h(1, "", "这是标题", "")
	'***************************************************
	Public Function H(Byval n, Byval id, Byval text, Byval args)
		Dim temp:temp=""
		Dim k:k=1
		If IsNumeric(n) Then k=CInt(k)
		If k<1 Then k=1
		If k>6 Then k=6
        temp = temp & "<h"&CInt(k)&"" & GetUpArgs(id, args, 1) & ">" & text & "</h"&CInt(k)&">"
		H = temp
	End Function

	'***************************************************
	'@ ab.html.h1(id, text, args)
	'@ html之h1标记
	'@ author:lajox; version:1.0.0 (2011-10-07)
	'==DEMO=============================================
	'@ ab.c.print ab.html.h1("", "这是最大标题", "")
	'***************************************************
	Public Function H1(Byval id, Byval text, Byval args)
		Dim temp:temp=""
        temp = temp & "<h1" & GetUpArgs(id, args, 1) & ">" & text & "</h1>"
		H1 = temp
	End Function

	'***************************************************
	'@ ab.html.hr(id, args)
	'@ html之hr水平线
	'@ author:lajox; version:1.0.0 (2011-10-07)
	'==DEMO=============================================
	'@ ab.c.print ab.html.hr("", "")
	'***************************************************
	Public Function Hr(Byval id, Byval text, Byval args)
		Dim temp:temp=""
        temp = temp & "<hr" & GetUpArgs(id, args, 1) & " />"
		Hr = temp
	End Function

	'***************************************************
	'@ ab.html.html(text, args)
	'@ html之html标记
	'@ author:lajox; version:1.0.0 (2011-10-08)
	'==DEMO=============================================
	'@ ab.c.print ab.html.html("<body>aaa</body>", "")
	'***************************************************
	Public Function Html(Byval text, Byval args)
		Dim temp:temp=""
        temp = temp & "<html" & GetUpArgs("", args, 1) & ">" & text & "</html>"
		Html = temp
	End Function

	'***************************************************
	'@ ab.html.head(text, args)
	'@ html之head标记
	'@ author:lajox; version:1.0.0 (2011-10-08)
	'==DEMO=============================================
	'@ ab.c.print ab.html.head("<title>标题</title>", "")
	'***************************************************
	Public Function Head(Byval text, Byval args)
		Dim temp:temp=""
        temp = temp & "<head" & GetUpArgs("", args, 1) & ">" & text & "</head>"
		Head = temp
	End Function

	'***************************************************
	'@ ab.html.title(text, args)
	'@ html之title标记
	'@ author:lajox; version:1.0.0 (2011-10-08)
	'==DEMO=============================================
	'@ ab.c.print ab.html.title("这是标题", "")
	'***************************************************
	Public Function Title(Byval text, Byval args)
		Dim temp:temp=""
        temp = temp & "<title" & GetUpArgs("", args, 1) & ">" & text & "</title>"
		Title = temp
	End Function

	'***************************************************
	'@ ab.html.body(text, args)
	'@ html之body标记
	'@ author:lajox; version:1.0.0 (2011-10-08)
	'==DEMO=============================================
	'@ ab.c.print ab.html.body("aaa", "")
	'***************************************************
	Public Function Body(Byval text, Byval args)
		Dim temp:temp=""
        temp = temp & "<body" & GetUpArgs("", args, 1) & ">" & text & "</body>"
		Body = temp
	End Function

	'***************************************************
	'@ ab.html.style(text, args)
	'@ html之style样式标记
	'@ author:lajox; version:1.0.1 (2011-11-01)
	'==DEMO=============================================
	'@ ab.c.print ab.html.style("body{background:#fff}", "media='screen'")
	'***************************************************
	Public Function Style(Byval text, Byval args)
		Dim temp:temp=""
        temp = temp & "<style type=""text/css""" & GetUpArgs("", args, 1) & ">" & text & "</style>"
		Style = temp
	End Function

	'***************************************************
	'@ ab.html.linkcss(text, args)
	'@ html之加载css样式文件
	'@ author:lajox; version:1.0.0 (2011-11-01)
	'==DEMO=============================================
	'@ ab.c.print ab.html.linkcss("/css/main.css", "")
	'***************************************************
	Public Function LinkCss(Byval src, Byval args)
		Dim temp:temp=""
		Dim strSrc:strSrc=""
		If Not IsNull(src) and trim(src)<>"" Then strSrc = src
		temp = temp & "<link rel=""stylesheet"" type=""text/css"" href=""" & strSrc & """" & GetUpArgs("", args, 1) & " />"
		LinkCss = temp
	End Function

	'***************************************************
	'@ ab.html.outcss(text, args)
	'@ html之外部加载css样式文件
	'@ author:lajox; version:1.0.0 (2011-12-06)
	'==DEMO=============================================
	'@ ab.c.print ab.html.outcss("/css/main.css", "")
	'***************************************************
	Public Function OutCss(Byval src, Byval args)
		Dim temp:temp=""
		Dim strSrc:strSrc=""
		If Not IsNull(src) and trim(src)<>"" Then strSrc = src
		temp = temp & "<style type=""text/css"" media=""screen""" & GetUpArgs("", args, 1) & ">@import url(" & strSrc & ");</style>"
		OutCss = temp
	End Function

	'***************************************************
	'@ ab.html.script(type_, src, text, args)
	'@ html之script脚本标记
	'@ author:lajox; version:1.0.1 (2011-12-14)
	'==DEMO=============================================
	'@ ab.c.print ab.html.script("js", "", "alert(0)", "")
	'@ ab.c.print ab.html.script("js", "http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js", "", "charset=""utf-8"" defer")
	'@ ==MIME值允许有==
	'@ application/ecmascript, application/javascript, application/x-ecmascript, application/x-javascript, text/ecmascript,
	'@ text/javascript, text/jscript, text/livescript, text/tcl, text/x-ecmascript, text/x-javascript, text/vbscript, text/vbs
	'***************************************************
	Public Function Script(Byval type_, Byval src, Byval text, Byval args)
		Dim temp:temp=""
		Dim strSrc:strSrc=""
		Dim MIME:MIME=""
		Dim strType:strType=""
		Dim strLang:strLang=""
		If Not IsNull(src) and trim(src)<>"" Then strSrc = " src=""" & trim(src) & """"
		If type_="" or LCase(type_)="js" Then
			MIME="text/javascript"
		ElseIf LCase(type_)="vb" Then
			MIME="text/vbscript"
		Else
			MIME=type_
		End If
		Select Case LCase(MIME)
			Case "text/javascript":
				strType=" type=""text/javascript"""
				strLang=" language=""javascript"""
			Case "text/vbscript":
				strType=" type=""text/vbscript"""
				strLang=" language=""vbscript"""
			Case Else:
				strType=" type="""& MIME &""""
				strLang=""
		End Select
        temp = temp & "<script" & strType & strSrc & GetUpArgs("", args, 1) & ">" & text & "</script>"
		Script = temp
	End Function

	'***************************************************
	'@ ab.html.js(text)
	'@ html之js输出脚本语句
	'@ author:lajox; version:1.0.0 (2011-12-14)
	'==DEMO=============================================
	'@ ab.c.print ab.html.js("alert(0)")
	'***************************************************
	Public Function Js(Byval text)
		Dim temp:temp=""
        temp = temp & "<script type=""text/javascript"">" & text & "</script>"
		Js = temp
	End Function

	'***************************************************
	'@ ab.html.getjs(text)
	'@ html之包含js文件
	'@ author:lajox; version:1.0.0 (2011-12-14)
	'==DEMO=============================================
	'@ ab.c.print ab.html.getJs("http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js")
	'***************************************************
	Public Function getJs(Byval src)
		Dim temp:temp=""
		temp = temp & ab.html.script("js", "" & src, "", "")
		getJs = temp
	End Function

	'***************************************************
	'@ ab.html.alert(text)
	'@ html之js弹出一个javascript信息框
	'==DEMO=============================================
	'@ ab.html.alert "hello world!"
	'***************************************************
	Public Function Alert(Byval text)
		Dim temp:temp=""
		Dim s_alert : s_alert = "alert(""" & text & """)"
		If AB.C.IsInt(text) Then s_alert = "alert(" & text & ")"
        temp = temp & "<script type=""text/javascript"">"& s_alert &"</script>"
		AB.C.Print temp
	End Function

	'***************************************************
	'@ ab.html.object(id, text, args)
	'@ html之object对象
	'@ author:lajox; version:1.0.0 (2011-10-07)
	'==DEMO=============================================
	'@ ab.c.print ab.html.object("slider", "", "classid=""clsid:F08DF954-8592-11D1-B16A-00C0F0283628""")
	'***************************************************
	Public Function [Object](Byval id, Byval text, Byval args)
		Dim temp:temp=""
        temp = temp & "<object" & GetUpArgs(id, args, 1) & ">" & text & "</object>"
		[Object] = temp
	End Function

	'***************************************************
	'@ ab.html.iframe(src, text, args)
	'@ html之签入iframe框架对象
	'@ author:lajox; version:1.0.0 (2012-01-10)
	'@ desc: 当宽或高度参数没设置时,默认为100%
	'==DEMO=============================================
	'@ ab.c.print ab.html.iframe("http://www.baidu.com/", "", "", "", "")
	'@ ab.c.print ab.html.iframe("http://www.baidu.com/", "300", "150", "frame1", "onload=''")
	'@ ab.c.print ab.html.iframe("http://www.baidu.com/", 370, 290, "frame1", "frameborder=0 scrolling=auto style=""border:0px""")
	'@ ab.c.print ab.html.iframe("http://www.baidu.com/", 450, 370, "frame1", "align=middle marginwidth=0 marginheight=0 vspace=0 hspace=0 frameborder=0 scrolling=no")
	'@ ab.c.print ab.html.iframe("http://www.baidu.com/", 450, 370, "frame1", "allowtransparency=""true"" style=""background-color:transparent""") 'iframe背景透明效果
	'***************************************************
	Public Function IFrame(Byval src, Byval width, Byval height, Byval id, Byval args)
		Dim temp:temp=""
		Dim strSrc:strSrc="":If Not IsNull(src) and trim(src)<>"" Then strSrc = src
		Dim iwidth,iheight: iwidth=width: iheight=height
		Dim dWidth,dHeight: dWidth="100%": dHeight="100%" '默认一般为: 300 * 150 或 auto * auto 或 100% * 100%
		'If Trim(iwidth&"")="" Then iwidth=dWidth:If Trim(iheight&"")="" Then iheight=dHeight
		Dim swidth, sheight : swidth = "" : sheight = ""
		If Trim(iwidth&"")<>"" Then swidth=" width=""" & iwidth & """"
		If Trim(iheight&"")<>"" Then sheight=" height=""" & iheight & """"
		temp = temp & "<iframe src=""" & strSrc & """" & swidth & sheight & GetUpArgs(id, args, 1) & "></iframe>"
		IFrame = temp
	End Function

	'***************************************************
	'@ ab.html.param(name, value, args)
	'@ 生成Param元素
	'@ author:lajox; version:1.0.0 (2011-10-07)
	'==DEMO=============================================
	'@ ab.html.param("Max", "10", "") => <param name="Max" value="10" />
	'***************************************************
	Public Function Param(Byval name, Byval value, Byval args)
		Dim temp:temp=""
        temp = temp & "<param name=""" & name & """ value=""" & value & """" & GetUpArgs("", args, 1) & " />"
		Param = temp
	End Function

	'***************************************************
	'@ ab.html.params(p, args)
	'@ 批量生成Param元素
	'@ author:lajox; version:1.0.0 (2011-10-07)
	'==DEMO=============================================
	'@ ab.html.params("Min:0;Max:10","") => <param name="Min" value="0" /><param name="Max" value="10" />
	'@ ab.html.params("A;B","") => <param name="A" value="" /><param name="B" value="" />
	'***************************************************
	Public Function Params(Byval p, Byval args)
		dim strParam,strOut,strName,sVal,strSplit,intPos
		strSplit=";" : If Instr(p,",")>0 Then strSplit=","
		For Each strParam In Split(p,strSplit)
		    strName=strParam
		    sVal=""
		    intPos=Instr(strParam,":")
			If intPos>0 Then
				strName=Left(strParam,intPos-1)
				sVal=Mid(strParam,intPos+1)
			End If
			If trim(strParam)<>"" Then strOut = strOut & "<param name=""" & strName & """ value=""" & sVal & """" & GetUpArgs("", args, 1) & " />"
		Next
		Params = strOut
	End Function

	'***************************************************
	'@ ab.html.input(type_, id, value, args)
	'@ html之INPUT控件
	'@ author:lajox; version:1.0.0 (2011-10-06)
	'==DEMO=============================================
	'@ ab.c.print ab.html.input("text", "name", "value", "")
	'***************************************************
	Public Function INPUT(Byval type_, Byval id, Byval value, Byval args)
		If IsNull(type_) Or Trim(type_)="" Then type_ = "text"
		Dim temp:temp=""
        temp = temp & "<input type=""" & type_ & """ value=""" & value & """" & GetUpArgs(id, args, 1) & " />"
		INPUT = temp
	End Function

	'***************************************************
	'@ ab.html.textbox(id, value, args)
	'@ html文本域控件
	'@ author:lajox; version:1.0.0 (2011-10-06)
	'==DEMO=============================================
	'@ ab.c.print ab.html.textbox("name", "value", "")
	'***************************************************
	Public Function TextBox(Byval id, Byval value, Byval args)
		Dim temp:temp=""
        temp = temp & "<input type=""text"" value=""" & value & """" & GetUpArgs(id, args, 1) & " />"
		TextBox = temp
	End Function

	'***************************************************
	'@ ab.html.textarea(id, value, args)
	'@ html文本域控件
	'@ author:lajox; version:1.0.0 (2011-10-06)
	'==DEMO=============================================
	'@ ab.c.print ab.html.textarea("name", "value", "")
	'***************************************************
	Public Function TextArea(Byval id, Byval value, Byval args)
		Dim temp:temp=""
        temp = temp & "<textarea" & GetUpArgs(id, args, 1) & ">" & value & "</textarea>"
		TextArea = temp
	End Function

	'***************************************************
	'@ ab.html.hiddenfield(id, value, args)
	'@ html隐藏域控件
	'@ author:lajox; version:1.0.0 (2011-10-06)
	'==DEMO=============================================
	'@ ab.c.print ab.html.textbox("name", "value", "")
	'***************************************************
	Public Function HiddenField(Byval id, Byval value, Byval args)
		Dim temp:temp=""
        temp = temp & "<input type=""hidden"" value=""" & value & """" & GetUpArgs(id, args, 1) & " />"
		HiddenField = temp
	End Function

	'***************************************************
	'@ ab.html.button(id, value, args)
	'@ html之Button控件
	'@ author:lajox; version:1.0.0 (2011-10-06)
	'==DEMO=============================================
	'@ ab.c.print ab.html.button("btn", "确定", "onclick=""alert('警告！')""")
	'***************************************************
	Public Function Button(Byval id, Byval value, Byval args)
		Dim temp:temp=""
        temp = temp & "<input type=""button"" value=""" & value & """" & GetUpArgs(id, args, 1) & " />"
		Button = temp
	End Function

	'***************************************************
	'@ ab.html.resetbutton(id, value, args)
	'@ html之Reset按钮
	'@ author:lajox; version:1.0.0 (2011-10-06)
	'==DEMO=============================================
	'@ ab.c.print ab.html.resetbutton("reset", "重置", "")
	'***************************************************
	Public Function ResetButton(Byval id, Byval value, Byval args)
		Dim temp:temp=""
        temp = temp & "<input type=""reset"" value=""" & value & """" & GetUpArgs(id, args, 1) & " />"
		ResetButton = temp
	End Function

	'***************************************************
	'@ ab.html.submitbutton(id, value, args)
	'@ html之Submit按钮
	'@ author:lajox; version:1.0.0 (2011-10-06)
	'==DEMO=============================================
	'@ ab.c.print ab.html.submitbutton("submit", "提交", "")
	'***************************************************
	Public Function SubmitButton(Byval id, Byval value, Byval args)
		Dim temp:temp=""
        temp = temp & "<input type=""submit"" value=""" & value & """" & GetUpArgs(id, args, 1) & " />"
		SubmitButton = temp
	End Function

	'***************************************************
	'@ ab.html.filebutton(id, args)
	'@ html之Submit按钮
	'@ author:lajox; version:1.0.0 (2011-10-06)
	'==DEMO=============================================
	'@ ab.c.print ab.html.filebutton("file1", "")
	'***************************************************
	Public Function FileButton(Byval id, Byval args)
		Dim temp:temp=""
        temp = temp & "<input type=""file""" & GetUpArgs(id, args, 1) & " />"
		FileButton = temp
	End Function

	'***************************************************
	'@ ab.html.radiobutton(id, value, text, args)
	'@ html之RadioButton控件
	'@ author:lajox; version:1.0.0 (2011-10-06)
	'==DEMO=============================================
	'@ ab.c.print ab.html.radiobutton(",sex", "0", "男", "checked") & " " & ab.html.radiobutton(",sex", "1", "女", "")
	'***************************************************
	Public Function RadioButton(Byval id, Byval value, Byval text, Byval args)
		Dim temp:temp=""
        temp = temp & "<input type=""radio"" value=""" & value & """" & GetUpArgs(id, args, 1) & " />"& text
		RadioButton = temp
	End Function

	'***************************************************
	'@ ab.html.checkbox(id, value, text, args)
	'@ html之CheckBox控件
	'@ author:lajox; version:1.0.0 (2011-10-06)
	'==DEMO=============================================
	'@ ab.c.print ab.html.checkbox("keep", "1", "保留", "checked")
	'***************************************************
	Public Function CheckBox(Byval id, Byval value, Byval text, Byval args)
		Dim temp:temp=""
        temp = temp & "<input type=""checkbox"" value=""" & value & """" & GetUpArgs(id, args, 1) & " />"& text
		CheckBox = temp
	End Function

	' ***********************************************************************************
	' @ ab.html.DropDownList(id,opt,value,args):String
	' @ 生成Select元素
	'@ author:lajox; version:1.0.0 (2011-10-07)
	' ==DEMO=============================================================================
	' @ ab.c.print ab.html.DropDownList("form_mode", "title:标题,content:内容", "title", "onchange='alert(0)'")
	' ***********************************************************************************
	Public Function DropDownList(Byval id, Byval opt, Byval value, Byval args)
	    dim temp
	    temp=temp & "<select" & GetUpArgs(id, args, 1) & ">"
	    temp=temp & Options(opt, value)
	    temp=temp & "</select>"
	    DropDownList = temp
	End Function

	'***************************************************
	'@ ab.html.options(opt, sel)
	'@ 生成Option元素
	'@ author:lajox; version:1.0.0 (2011-10-07)
	'==DEMO=============================================
	'@ ab.html.options("FJ:福建;ZJ:浙江","ZJ") => <option value="FJ">福建</option><option value="ZJ" selected>浙江</option>
	'@ ab.html.options("福建;浙江","浙江") => <option value="福建">福建</option><option value="浙江" selected>浙江</option>
	'@ ab.html.options("FJ:福建","") => <option value="FJ">福建</option>
	'***************************************************
	Public Function Options(Byval opt, Byval sel)
		dim strOption,strSel,strOut,strText,sVal,strSplit,intPos
		strSplit=";" : If Instr(opt,",")>0 Then strSplit=","
		For Each strOption In Split(opt,strSplit)
		    strText=strOption
		    sVal=strOption
		    intPos=Instr(strOption,":")
			If intPos>0 Then
				sVal=Left(strOption,intPos-1)
				strText=Mid(strOption,intPos+1)
			End If
			strSel="":If sVal=sel Then strSel=" selected"
			If trim(strOption)<>"" Then strOut = strOut & "<option value=""" & sVal & """" & strSel & ">" & strText & "</option>"
		Next
		Options = strOut
	End Function

	' ***********************************************************************************
	' @ ab.html.Optgroup(label,id,opt,value,args):String
	' @ 生成Optgroup下拉列表中的一组选项
	'@ author:lajox; version:1.0.0 (2011-10-07)
	' ==DEMO=============================================================================
	' @ ab.c.print ab.html.Optgroup("search", "", "title:标题,content:内容", "title", "class='' style='' title=''")
	' ***********************************************************************************
	Public Function Optgroup(Byval label, Byval id, Byval opt, Byval value, Byval args)
	    dim temp
		Dim sLabel:sLabel=""
		If Not IsNull(label) and label<>"" Then sLabel=" label=""" & label & """"
	    temp=temp & "<optgroup" & sLabel & GetUpArgs(id, args, 1) & ">"
	    temp=temp & Options(opt, value)
	    temp=temp & "</optgroup>"
	    Optgroup = temp
	End Function

	'***************************************************
	'@ ab.html.tagb(tag, id, args)
	'@ 标签开始
	'@ author:lajox; version:1.0.0 (2011-10-07)
	'==DEMO=============================================
	'@ ab.c.print ab.html.tagb("div", "wrapper", "") & "---" & ab.html.tage("div")
	'***************************************************
	Public Function TagB(Byval tag, Byval id, Byval args)
		Dim temp:temp=""
        temp = temp & "<" & tag & GetUpArgs(id, args, 1) & ">"
	    TagB = temp
	End Function

	'***************************************************
	'@ ab.html.tage(tag)
	'@ 闭合标签
	'@ author:lajox; version:1.0.0 (2011-10-07)
	'==DEMO=============================================
	'@ ab.c.print ab.html.tagb("div", "wrapper", "") & "---" & ab.html.tage("div")
	'***************************************************
	Public Function TagE(Byval tag)
		Dim temp:temp=""
        temp = temp & "</" & tag & ">"
	    TagE = temp
	End Function

	'***************************************************
	'@ ab.html.tagbe(tag, id, args)
	'@ 单标记标签
	'@ author:lajox; version:1.0.0 (2011-10-09)
	'==DEMO=============================================
	'@ ab.c.print ab.html.tagbe("br", "", "")
	'***************************************************
	Public Function TagBE(Byval tag, Byval id, Byval args)
		Dim temp:temp=""
        temp = temp & "<" & tag & GetUpArgs(id, args, 1) & " />"
	    TagBE = temp
	End Function

	'***************************************************
	'@ ab.html.tag(args)
	'@ 简单HTML标记
	'@ author:lajox; version:1.0.0 (2011-10-08)
	'==DEMO=============================================
	'@ ab.c.print ab.html.tag("<div>aaa</div>")
	'***************************************************
	Public Function Tag(Byval args)
		Dim temp:temp=""
        temp = temp & args
	    Tag = temp
	End Function

	' ============== 以下辅助函数 ===================

	Private Function GetUpArgs(Byval id, Byval args, Byval p)
		On Error Resume Next
		IF Err Then Err.Clear
		Dim temp:temp=""
		Dim strId, strArgs
		If IsNull(id) Or Trim(id)="" Then id = ""
		If IsNull(args) Or Trim(args)="" Then args = ""
		IF Trim(id)<>"" Then
			If InStr(id,",")>0 Then
				If Trim(Split(id,",")(0))<>"" Then strId = strId & " id=""" & Trim(Split(id, ",")(0)) & """"
				If Trim(Split(id,",")(1))<>"" Then strId = strId & " name=""" & Trim(Split(id, ",")(1)) & """"
			Else
				strId = strId & " id=""" & Trim(id) & """"
				strId = strId & " name=""" & Trim(id) & """"
			End IF
		End IF
		Dim pt:pt=0
		If IsNumeric(p) Then pt=CLng(p)
		If pt=0 then
			IF Trim(args)<>"" Then strArgs = " " & args & ""
		Else
			IF Trim(args)<>"" Then
				If Instr(args,"{")>0 Then args = Replace(Replace(Replace(args,"\\",Chr(29)),"\{",Chr(30)),"\}",Chr(31))
				Dim o_str:o_str=trim(args)
				Dim o_style,o_class,o_title,o_alt,o_extra
				Dim RegEx,Match,Matches,sKey,sVal
				Set RegEx = AB.C.RegMatch(args, "([a-z0-9_\-]+)\s*\{([^\{}]*)\}")
				For Each Match In RegEx
					sKey=Match.SubMatches(0)
					sVal=Match.SubMatches(1)
					Select Case sKey
						Case "style"
							If trim(sVal)<>"" then o_style = " style=""" & trim(sVal) & """"
							o_style = Replace(Replace(Replace(o_style,Chr(29),"\\"),Chr(30),"\{"),Chr(31),"\}")
							o_str = Replace(o_str, Match.Value, o_style)
						Case "class"
							If trim(sVal)<>"" then o_class = " class=""" & trim(sVal) & """"
							o_class = Replace(Replace(Replace(o_class,Chr(29),"\\"),Chr(30),"\{"),Chr(31),"\}")
							o_str = Replace(o_str, Match.Value, o_class)
						Case "title"
							If trim(sVal)<>"" then o_title = " title=""" & trim(sVal) & """"
							o_title = Replace(Replace(Replace(o_title,Chr(29),"\\"),Chr(30),"\{"),Chr(31),"\}")
							o_str = Replace(o_str, Match.Value, o_title)
						Case "alt"
							If trim(sVal)<>"" then o_alt = " alt=""" & trim(sVal) & """"
							o_alt = Replace(Replace(Replace(o_alt,Chr(29),"\\"),Chr(30),"\{"),Chr(31),"\}")
							o_str = Replace(o_str, Match.Value, o_alt)
						Case Else
							If trim(sVal)<>"" then o_extra = " "&sKey&"=""" & trim(sVal) & """"
							o_extra = Replace(Replace(Replace(o_extra,Chr(29),"\\"),Chr(30),"\{"),Chr(31),"\}")
							o_str = Replace(o_str, Match.Value, o_extra)
					End Select
					'strArgs = strArgs & o_style & o_class & o_title & o_alt & o_extra
				Next
				Set RegEx = Nothing
				strArgs = " " & o_str
			End IF
		End If
		temp = strId & "" & strArgs
		GetUpArgs = temp
		On Error Goto 0
	End Function

End Class
%>