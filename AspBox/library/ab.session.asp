<%
'######################################################################
'## ab.session.asp
'## -------------------------------------------------------------------
'## Feature     :   AspBox Session Class
'## Version     :   v1.0
'## Author      :   Lajox(lajox@19www.com)
'## Update Date :   2014/06/25 0:28
'## Description :   AspBox Session操作模块
'######################################################################

Class Cls_AB_Session

	Private	s_mark

	Private Sub Class_Initialize()
		s_mark = ""
	End Sub

	Private Sub Class_Terminate()

	End Sub

	'@ *****************************************************************************************
	'@ 属性值:  AB.Session.Mark [= s]
	'@ 返  回:  --
	'@ 作  用:  设置/获取 Session键值前缀
	'==DESC=====================================================================================
	'@ 参数 s(可选): String (字符串)
	'==DEMO=====================================================================================
	'@ AB.Session.Mark = "abxss_"
	'@ AB.C.PrintCn "sesson键值前缀:" & AB.Session.Mark
	'@ AB.Session.Set "temp", "abcd"
	'@ AB.C.PrintCn AB.Session("temp")
	'@ AB.C.PrintCn "这将等同于:" & Session(AB.Session.Mark & "temp")
	'@ *****************************************************************************************

	Public Property Let Mark(Byval s)
		s_mark = s
	End Property

	Public Property Get Mark()
		Mark = s_mark
	End Property

	'@ *****************************************************************************************
	'@ 过程名:  AB.Session.Timeout = value
	'@ 返  回:  --
	'@ 作  用:  设置 Session的Timeout属性值
	'==DESC=====================================================================================
	'@ 参数 value: Integer (整数)
	'==DEMO=====================================================================================
	'@ AB.Session.Timeout = 500
	'@ *****************************************************************************************

    Public Property Let Timeout(Byval value)
		If IsNumeric(value) Then Session.Timeout = value
    End Property

	'@ *****************************************************************************************
	'@ 过程名:  AB.Session.Find(key) 缩写形式 AB.Session(key)
	'@ 返  回:  Anything (任意值)
	'@ 作  用:  取Session值
	'==DESC=====================================================================================
	'@ 参数 key: String (字符串)
	'==DEMO=====================================================================================
	'@ ab.c.print AB.Session("s_admin")
	'@ *****************************************************************************************

    Public Default Property Get Find(Byval key)
        Find = [Get](key)
    End Property

	'@ *****************************************************************************************
	'@ 过程名:  AB.Session.Get(key)
	'@ 返  回:  Anything (任意值)
	'@ 作  用:  取Session值
	'==DESC=====================================================================================
	'@ 参数 key: String (字符串)
	'==DEMO=====================================================================================
	'@ ab.c.print AB.Session.Get("s_admin")
	'@ *****************************************************************************************

	Function [Get](Byval key)
		If IsObject(Session(Me.Mark & key)) Then
			Set [Get] = Session(Me.Mark & key)
		Else
			[Get] = Session(Me.Mark & key)
		End If
    End Function

	'@ *****************************************************************************************
	'@ 过程名:  AB.Session.Set key, value
	'@ 返  回:  无返回值
	'@ 作  用:  设置Session值
	'==DESC=====================================================================================
	'@ 参数 key: String (字符串)
	'@ 参数 value: Anything (任意值)
	'==DEMO=====================================================================================
	'@ AB.Session.Set "s_admin", "admin"
	'@ *****************************************************************************************

	Sub [Set](Byval key, Byval value)
		If IsObject(value) Then
			Set Session(Me.Mark & key) = value
		Else
			Session(Me.Mark & key) = value
		End If
    End Sub

	'@ *****************************************************************************************
	'@ 过程名:  AB.Session.Remove key
	'@ 返  回:  无返回值
	'@ 作  用:  删除某个Session值
	'==DESC=====================================================================================
	'@ 参数 key: String (字符串)
	'==DEMO=====================================================================================
	'@ AB.Session.Remove "s_admin"
	'@ *****************************************************************************************

	Sub Remove(Byval key)
		If IsObject(Session(Me.Mark & key)) Then
			Set Session(Me.Mark & key) = Nothing
		End If
        Session.Contents.Remove(Me.Mark & key)
    End Sub

	'@ *****************************************************************************************
	'@ 过程名:  AB.Session.RemoveAll
	'@ 返  回:  无返回值
	'@ 作  用:  清空删除所有Session值
	'==DESC=====================================================================================
	'@ 参数: 无
	'==DEMO=====================================================================================
	'@ AB.Session.RemoveAll()
	'@ *****************************************************************************************

	Sub RemoveAll()
        Dim item
        For Each item In Session.Contents
			'Me.Remove(Right(item,Len(item)-Len(Me.Mark&"")))
			Session.Contents.Remove(item)
        Next
		Session.Abandon()
	End Sub

	'@ *****************************************************************************************
	'@ 过程名:  AB.Session.Compare(key1, key2)
	'@ 返  回:  True/False 布尔值
	'@ 作  用:  比较两个Session值是否相等
	'==DESC=====================================================================================
	'@ 参数 key1: String (字符串) Session 1 的键值
	'@ 参数 key2: String (字符串) Session 2 的键值
	'==DEMO=====================================================================================
	'@ AB.C.Print AB.Session.Compare("s_v1", "s_v2")
	'@ *****************************************************************************************

	Function [Compare](Byval key1, Byval key2)
        Dim Cache1
        Cache1 = Me.[Get](key1)
        Dim Cache2
        Cache2 = Me.[Get](key2)
        If TypeName(Cache1) <> TypeName(Cache2) Then
            Compare = False
        Else
            If TypeName(Cache1) = "Object" Then
                Compare = (Cache1 Is Cache2)
            Else
                If TypeName(Cache1) = "Variant()" Then
                    Compare = (Join(Cache1, "^") = Join(Cache2, "^"))
                Else
                    Compare = (Cache1 = Cache2)
                End If
            End If
        End If
    End Function

End Class
%>