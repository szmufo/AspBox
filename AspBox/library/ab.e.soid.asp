<%
'######################################################################
'## ab.e.soid.asp
'## -------------------------------------------------------------------
'## Feature     :   SoID Encryption
'## Version     :   v1.0
'## Author      :   Lajox(lajox@19www.com)
'## Update Date :   2011/09/27 9:32
'## Description :   AspBox SoID Encryption Block
'## ====================
'## 只支持纯数值加密
'######################################################################

Class Cls_AB_E_SoID

	Private s_pass

	Private Sub Class_Initialize()
		s_pass = "3682491570"
	End Sub

	Private Sub Class_Terminate()

	End Sub

	'-----------------------------------------------------------
	' @ 设置SoID加密解密密钥，全局参数，读写
	'-----------------------------------------------------------
	' Desc: 设置使用SoID加密解密时的密钥(0-9数字组成)
	' e.g. AB.E.SoID.Password = "3682491570"
	'-----------------------------------------------------------

	Public Property Let Password(ByVal p)
		If (Not IsNull(p)) and p<>"" Then s_pass = p
	End Property

	Public Property Get Password()
		Password = s_pass
	End Property

	'@ ******************************************************************
	'@ 过程名:  AB.E.SoID.E(s) {简写为： AB.E.SoID(s) }
	'@ 返  回:  (SoIDEnc加密算法)加密后的字符串
	'@ 作  用:  对字符串进行加密(由 0-9 等字符组成)
	'==Param==============================================================
	'@ s  : 待加密的数值
	'==DEMO==============================================================
	'@ AB.E.SoID.E("2011") => 366432513
	'@ ******************************************************************

	Public Default Function E(Byval s)
		E = SoIDEnc(s)
	End Function

	'@ ******************************************************************
	'@ 过程名:  AB.E.SoID.D(s)
	'@ 返  回:  对(由SoIDEnc加密算法)加密的字符串进行解密还原
	'@ 作  用:  解密(由SoIDEnc加密算法)的字符串
	'==Param==============================================================
	'@ s  : 待解密的字符串
	'==DEMO==============================================================
	'@ AB.E.SoID.D(AB.E.SoID.E("2011")) '返回值: 2011
	'@ ******************************************************************

	Public Function D(Byval s)
		D = SoIDDec(s)
	End Function

	Private Function SoIDEnc(ByVal n)
		'@ *************************************
		'@ 加密ID,参数必须是数字
		'@ SoIDEnc(n):Int
		'@ =====================================
		'@ 只支持纯数字加密, 加密失败返回原值
		'@ *************************************
		Dim sn: sn = Trim(n) & ""
		If AB.C.IsNum(sn)=False Or sn="" Then:SoIDEnc=n:Exit Function:End If
		Dim sLib,intRnd1,intRnd2,intText,intPos,I,temp
		sLib = s_pass & ""
		Randomize
		intRnd1 = round(rnd()*899)+100
		intRnd2 = round(rnd()*84)
		intText = intRnd1 + sn - 84 
		intRnd1 = intRnd1 - intRnd2   
		intRnd1 = Right("00" & intRnd1,3)
		intRnd2 = Right("0" & intRnd2,2)
		intText = intRnd1 & intText & intRnd2
		temp = ""
		For I=1 To Len(intText)
			intPos = Instr(sLib,Mid(intText,I,1))
			If intPos=10 Then intPos=0
			temp = temp & intPos
		Next
		SoIDEnc = Cstr(temp)
	End Function

	Private Function SoIDDec(ByVal n)
		'@ ****************************************
		'@ 解密ID,参数必须是经过soidenc加密过的数值
		'@ SoIDDec(n):Int
		'==DESC====================================
		'@ 如果是伪造的或解密失败则返回原值
		'@ ****************************************
		Dim sn: sn = Trim(n) & ""
		If Len(sn)<5 Or AB.C.IsNum(sn)=False Then:SoIDDec=n:Exit Function:End If
		dim sLib,intRnd1,intRnd2,intText,intPos,I,temp
		sLib = s_pass & ""
		intText = ""
		For I=1 To Len(sn)
			intPos = Mid(sn,I,1)
			If intPos=0 Then intPos=10			
			intText = intText & Mid(sLib,intPos,1)
		Next
		intRnd1 = Left(intText,3)
		intRnd2 = Right(intText,2)
		intRnd1 = Abs(intRnd1) + Abs(intRnd2)
		intText = Mid(intText, 4, Len(intText)-5)
		If AB.C.IsNum(intText)=False Then:SoIDDec=n:Exit Function:End If
		temp = Abs(intText) + 84 - Abs(intRnd1)
		SoIDDec = temp  
	End Function

End Class
%>