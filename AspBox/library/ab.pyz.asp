<%
'######################################################################
'## ab.pyz.asp
'## -------------------------------------------------------------------
'## Feature     :   AspBox PinYin Char Transfer Class
'## Version     :   v1.0
'## Author      :   Lajox(lajox@19www.com)
'## Update Date :   2011/09/30 16:10
'## Description :   AspBox PinYin Char Transfer(普通版)
'----------------------------------------------------------------
'函数列表: 
'	AB.Lib("PYZ").PinYin("拼音")
'   AB.Lib("PYZ").ChangePY("拼音")
'######################################################################

Class Cls_AB_PYZ

	Private d

	Private Sub Class_Initialize()
		Call DictAdd()
	End Sub

	Public Sub Class_Terminate()

	End Sub

	Public Default Function PinYin(Byval s)
		Dim p : p = s
		p = RelaceSomeChar(p) '处理特殊字符
		p = getEnglish(p) '自定义转换
		p = ChangePY(p) '按字典转换(不够智能)
		PinYin = p
	End Function

	Public Function ChangePY(Byval s)
		Dim p:p=s
		Dim temp:temp=""
		If IsNull(p) Or p="" Then
			temp = ""
		Else
			temp = c(p)
		End IF
		ChangePY = temp
	End Function

	Function getEnglish(ByVal s)
		p = s
		p = Replace(p, "中国", "china")
		p = Replace(p, "策划", "plan")
		p = Replace(p, "免费", "free")
		p = Replace(p, "介绍", "intro")
		p = Replace(p, "技巧", "skill")
		p = Replace(p, "生活", "life")
		p = Replace(p, "活动", "activity")
		p = Replace(p, "工具", "tool")
		p = Replace(p, "联盟", "union")
		p = Replace(p, "注册", "register")
		p = Replace(p, "经验", "experience")
		p = Replace(p, "翻译", "translate")
		p = Replace(p, "项目", "item")
		p = Replace(p, "网站", "web-site")
		p = Replace(p, "英语", "english")
		p = Replace(p, "英文", "english")
		p = Replace(p, "雅虎", "yahoo")
		p = Replace(p, "新浪", "sina")
		p = Replace(p, "支付宝", "alipay")
		p = Replace(p, "交易", "trade")
		p = Replace(p, "网店", "b2c")
		p = Replace(p, "升级", "update")
		p = Replace(p, "杂志", "magazine")
		p = Replace(p, "空间", "space")
		p = Replace(p, "爱情", "love")
		p = Replace(p, "朋友", "friend")
		p = Replace(p, "友情", "friend")
		p = Replace(p, "链接", "like")
		p = Replace(p, "标签", "label")
		p = Replace(p, "运行", "running")
		p = Replace(p, "管理", "manager")
		p = Replace(p, "管理", "manage")
		p = Replace(p, "页面", "page")
		p = Replace(p, "模板", "template")
		p = Replace(p, "游戏", "game")
		p = Replace(p, "论坛", "forum")
		p = Replace(p, "新闻", "news")
		p = Replace(p, "音乐", "music")
		p = Replace(p, "帮助", "help")
		p = Replace(p, "优化", "optimize")
		p = Replace(p, "软件", "soft")
		p = Replace(p, "教程", "tech")
		p = Replace(p, "下载", "download")
		p = Replace(p, "搜索", "search")
		p = Replace(p, "引擎", "engine")
		p = Replace(p, "蜘蛛", "spider")
		p = Replace(p, "日志", "log")
		p = Replace(p, "博客", "blog")
		p = Replace(p, "百度", "baidu")
		p = Replace(p, "谷歌", "google")
		p = Replace(p, "邮箱", "mailbox")
		p = Replace(p, "邮件", "mail")
		p = Replace(p, "域名", "domain")
		p = Replace(p, "测试", "test")
		p = Replace(p, "演示", "demo")
		p = Replace(p, "音乐", "music")
		p = Replace(p, "笑话", "joke")
		p = Replace(p, "产品", "product")
		p = Replace(p, "留言", "message")
		p = Replace(p, "反馈", "freedback")
		p = Replace(p, "评论", "comment")
		p = Replace(p, "推荐", "commend")
		p = Replace(p, "共享", "share")
		p = Replace(p, "资源", "resource")
		p = Replace(p, "插件", "plugins")
		p = Replace(p, "本本", "notebook")
		p = Replace(p, "电脑", "computer")
		p = Replace(p, "系统", "system")
		p = Replace(p, "学校", "school")
		p = Replace(p, "无忧", "5u")
		p = Replace(p, "工作", "job")
		p = Replace(p, "信息", "info")
		p = Replace(p, "娱乐", "ent")
		p = Replace(p, "汽车", "car")
		p = Replace(p, "手机", "mobile")
		p = Replace(p, "网络", "network")
		p = Replace(p, "老板", "boss")
		p = Replace(p, "狗", "dog")
		p = Replace(p, "电视", "tv")
		p = Replace(p, "电影", "movie")
		getEnglish = p
	End Function

	Function RelaceSomeChar(Byval s)
		Dim p:p=s
		p = replace(p, "/", "")
		p = replace(p, "\", "")
		p = replace(p, "*", "")
		p = replace(p, "]", "")
		p = replace(p, "[", "")
		p = replace(p, "}", "")
		p = replace(p, "{", "")
		p = replace(p, "'", "")
		RelaceSomeChar = p
	End Function

	'取得汉字首拼
	Function GetFirstPY(Byval s)
		Dim p:p=s
		Dim py, tmp, str, resultstr
		for i = 1 to len(p)
			py = ""
			str = mid(p,i,1)
			tmp = 65536 + asc(str)
			if(tmp>=45217 and tmp<=45252) then 
				py = py & "A" 
			elseif(tmp>=45253 and tmp<=45760) then 
				py = py & "B" 
			elseif(tmp>=45761 and tmp<=46317) then 
				py = py & "C" 
			elseif(tmp>=46318 and tmp<=46825) then 
				py = py & "D" 
			elseif(tmp>=46826 and tmp<=47009) then 
				py = py & "E" 
			elseif(tmp>=47010 and tmp<=47296) then 
				py = py & "F" 
			elseif(tmp>=47297 and tmp<=47613) then 
				py = py & "G" 
			elseif(tmp>=47614 and tmp<=48118) then 
				py = py & "H" 
			elseif(tmp>=48119 and tmp<=49061) then 
				py = py & "J" 
			elseif(tmp>=49062 and tmp<=49323) then 
				py = py & "K" 
			elseif(tmp>=49324 and tmp<=49895) then 
				py = py & "L" 
			elseif(tmp>=49896 and tmp<=50370) then 
				py = py & "M" 
			elseif(tmp>=50371 and tmp<=50613) then 
				py = py & "N" 
			elseif(tmp>=50614 and tmp<=50621) then 
				py = py & "O" 
			elseif(tmp>=50622 and tmp<=50905) then 
				py = py & "P" 
			elseif(tmp>=50906 and tmp<=51386) then 
				py = py & "Q" 
			elseif(tmp>=51387 and tmp<=51445) then 
				py = py & "R" 
			elseif(tmp>=51446 and tmp<=52217) then 
				py = py & "S" 
			elseif(tmp>=52218 and tmp<=52697) then 
				py = py & "T" 
			elseif(tmp>=52698 and tmp<=52979) then 
				py = py & "W" 
			elseif(tmp>=52980 and tmp<=53688) then 
				py = py & "X" 
			elseif(tmp>=53689 and tmp<=54480) then 
				py = py & "Y" 
			elseif(tmp>=54481 and tmp<=56289) then 
				py = py & "Z" 
			end if

			If py = "" then
				py = str
			End If
			Resultstr = Resultstr & py
		next
		GetFirstPY = resultstr
	End Function

	Private Sub DictAdd
		Set d = CreateObject(AB.DictName)
		d.add "a",-20319
		d.add "ai",-20317
		d.add "an",-20304
		d.add "ang",-20295
		d.add "ao",-20292
		d.add "ba",-20283
		d.add "bai",-20265
		d.add "ban",-20257
		d.add "bang",-20242
		d.add "bao",-20230
		d.add "bei",-20051
		d.add "ben",-20036
		d.add "beng",-20032
		d.add "bi",-20026
		d.add "bian",-20002
		d.add "biao",-19990
		d.add "bie",-19986
		d.add "bin",-19982
		d.add "bing",-19976
		d.add "bo",-19805
		d.add "bu",-19784
		d.add "ca",-19775
		d.add "cai",-19774
		d.add "can",-19763
		d.add "cang",-19756
		d.add "cao",-19751
		d.add "ce",-19746
		d.add "ceng",-19741
		d.add "cha",-19739
		d.add "chai",-19728
		d.add "chan",-19725
		d.add "chang",-19715
		d.add "chao",-19540
		d.add "che",-19531
		d.add "chen",-19525
		d.add "cheng",-19515
		d.add "chi",-19500
		d.add "chong",-19484
		d.add "chou",-19479
		d.add "chu",-19467
		d.add "chuai",-19289
		d.add "chuan",-19288
		d.add "chuang",-19281
		d.add "chui",-19275
		d.add "chun",-19270
		d.add "chuo",-19263
		d.add "ci",-19261
		d.add "cong",-19249
		d.add "cou",-19243
		d.add "cu",-19242
		d.add "cuan",-19238
		d.add "cui",-19235
		d.add "cun",-19227
		d.add "cuo",-19224
		d.add "da",-19218
		d.add "dai",-19212
		d.add "dan",-19038
		d.add "dang",-19023
		d.add "dao",-19018
		d.add "de",-19006
		d.add "deng",-19003
		d.add "di",-18996
		d.add "dian",-18977
		d.add "diao",-18961
		d.add "die",-18952
		d.add "ding",-18783
		d.add "diu",-18774
		d.add "dong",-18773
		d.add "dou",-18763
		d.add "du",-18756
		d.add "duan",-18741
		d.add "dui",-18735
		d.add "dun",-18731
		d.add "duo",-18722
		d.add "e",-18710
		d.add "en",-18697
		d.add "er",-18696
		d.add "fa",-18526
		d.add "fan",-18518
		d.add "fang",-18501
		d.add "fei",-18490
		d.add "fen",-18478
		d.add "feng",-18463
		d.add "fo",-18448
		d.add "fou",-18447
		d.add "fu",-18446
		d.add "ga",-18239
		d.add "gai",-18237
		d.add "gan",-18231
		d.add "gang",-18220
		d.add "gao",-18211
		d.add "ge",-18201
		d.add "gei",-18184
		d.add "gen",-18183
		d.add "geng",-18181
		d.add "gong",-18012
		d.add "gou",-17997
		d.add "gu",-17988
		d.add "gua",-17970
		d.add "guai",-17964
		d.add "guan",-17961
		d.add "guang",-17950
		d.add "gui",-17947
		d.add "gun",-17931
		d.add "guo",-17928
		d.add "ha",-17922
		d.add "hai",-17759
		d.add "han",-17752
		d.add "hang",-17733
		d.add "hao",-17730
		d.add "he",-17721
		d.add "hei",-17703
		d.add "hen",-17701
		d.add "heng",-17697
		d.add "hong",-17692
		d.add "hou",-17683
		d.add "hu",-17676
		d.add "hua",-17496
		d.add "huai",-17487
		d.add "huan",-17482
		d.add "huang",-17468
		d.add "hui",-17454
		d.add "hun",-17433
		d.add "huo",-17427
		d.add "ji",-17417
		d.add "jia",-17202
		d.add "jian",-17185
		d.add "jiang",-16983
		d.add "jiao",-16970
		d.add "jie",-16942
		d.add "jin",-16915
		d.add "jing",-16733
		d.add "jiong",-16708
		d.add "jiu",-16706
		d.add "ju",-16689
		d.add "juan",-16664
		d.add "jue",-16657
		d.add "jun",-16647
		d.add "ka",-16474
		d.add "kai",-16470
		d.add "kan",-16465
		d.add "kang",-16459
		d.add "kao",-16452
		d.add "ke",-16448
		d.add "ken",-16433
		d.add "keng",-16429
		d.add "kong",-16427
		d.add "kou",-16423
		d.add "ku",-16419
		d.add "kua",-16412
		d.add "kuai",-16407
		d.add "kuan",-16403
		d.add "kuang",-16401
		d.add "kui",-16393
		d.add "kun",-16220
		d.add "kuo",-16216
		d.add "la",-16212
		d.add "lai",-16205
		d.add "lan",-16202
		d.add "lang",-16187
		d.add "lao",-16180
		d.add "le",-16171
		d.add "lei",-16169
		d.add "leng",-16158
		d.add "li",-16155
		d.add "lia",-15959
		d.add "lian",-15958
		d.add "liang",-15944
		d.add "liao",-15933
		d.add "lie",-15920
		d.add "lin",-15915
		d.add "ling",-15903
		d.add "liu",-15889
		d.add "long",-15878
		d.add "lou",-15707
		d.add "lu",-15701
		d.add "lv",-15681
		d.add "luan",-15667
		d.add "lue",-15661
		d.add "lun",-15659
		d.add "luo",-15652
		d.add "ma",-15640
		d.add "mai",-15631
		d.add "man",-15625
		d.add "mang",-15454
		d.add "mao",-15448
		d.add "me",-15436
		d.add "mei",-15435
		d.add "men",-15419
		d.add "meng",-15416
		d.add "mi",-15408
		d.add "mian",-15394
		d.add "miao",-15385
		d.add "mie",-15377
		d.add "min",-15375
		d.add "ming",-15369
		d.add "miu",-15363
		d.add "mo",-15362
		d.add "mou",-15183
		d.add "mu",-15180
		d.add "na",-15165
		d.add "nai",-15158
		d.add "nan",-15153
		d.add "nang",-15150
		d.add "nao",-15149
		d.add "ne",-15144
		d.add "nei",-15143
		d.add "nen",-15141
		d.add "neng",-15140
		d.add "ni",-15139
		d.add "nian",-15128
		d.add "niang",-15121
		d.add "niao",-15119
		d.add "nie",-15117
		d.add "nin",-15110
		d.add "ning",-15109
		d.add "niu",-14941
		d.add "nong",-14937
		d.add "nu",-14933
		d.add "nv",-14930
		d.add "nuan",-14929
		d.add "nue",-14928
		d.add "nuo",-14926
		d.add "o",-14922
		d.add "ou",-14921
		d.add "pa",-14914
		d.add "pai",-14908
		d.add "pan",-14902
		d.add "pang",-14894
		d.add "pao",-14889
		d.add "pei",-14882
		d.add "pen",-14873
		d.add "peng",-14871
		d.add "pi",-14857
		d.add "pian",-14678
		d.add "piao",-14674
		d.add "pie",-14670
		d.add "pin",-14668
		d.add "ping",-14663
		d.add "po",-14654
		d.add "pu",-14645
		d.add "qi",-14630
		d.add "qia",-14594
		d.add "qian",-14429
		d.add "qiang",-14407
		d.add "qiao",-14399
		d.add "qie",-14384
		d.add "qin",-14379
		d.add "qing",-14368
		d.add "qiong",-14355
		d.add "qiu",-14353
		d.add "qu",-14345
		d.add "quan",-14170
		d.add "que",-14159
		d.add "qun",-14151
		d.add "ran",-14149
		d.add "rang",-14145
		d.add "rao",-14140
		d.add "re",-14137
		d.add "ren",-14135
		d.add "reng",-14125
		d.add "ri",-14123
		d.add "rong",-14122
		d.add "rou",-14112
		d.add "ru",-14109
		d.add "ruan",-14099
		d.add "rui",-14097
		d.add "run",-14094
		d.add "ruo",-14092
		d.add "sa",-14090
		d.add "sai",-14087
		d.add "san",-14083
		d.add "sang",-13917
		d.add "sao",-13914
		d.add "se",-13910
		d.add "sen",-13907
		d.add "seng",-13906
		d.add "sha",-13905
		d.add "shai",-13896
		d.add "shan",-13894
		d.add "shang",-13878
		d.add "shao",-13870
		d.add "she",-13859
		d.add "shen",-13847
		d.add "sheng",-13831
		d.add "shi",-13658
		d.add "shou",-13611
		d.add "shu",-13601
		d.add "shua",-13406
		d.add "shuai",-13404
		d.add "shuan",-13400
		d.add "shuang",-13398
		d.add "shui",-13395
		d.add "shun",-13391
		d.add "shuo",-13387
		d.add "si",-13383
		d.add "song",-13367
		d.add "sou",-13359
		d.add "su",-13356
		d.add "suan",-13343
		d.add "sui",-13340
		d.add "sun",-13329
		d.add "suo",-13326
		d.add "ta",-13318
		d.add "tai",-13147
		d.add "tan",-13138
		d.add "tang",-13120
		d.add "tao",-13107
		d.add "te",-13096
		d.add "teng",-13095
		d.add "ti",-13091
		d.add "tian",-13076
		d.add "tiao",-13068
		d.add "tie",-13063
		d.add "ting",-13060
		d.add "tong",-12888
		d.add "tou",-12875
		d.add "tu",-12871
		d.add "tuan",-12860
		d.add "tui",-12858
		d.add "tun",-12852
		d.add "tuo",-12849
		d.add "wa",-12838
		d.add "wai",-12831
		d.add "wan",-12829
		d.add "wang",-12812
		d.add "wei",-12802
		d.add "wen",-12607
		d.add "weng",-12597
		d.add "wo",-12594
		d.add "wu",-12585
		d.add "xi",-12556
		d.add "xia",-12359
		d.add "xian",-12346
		d.add "xiang",-12320
		d.add "xiao",-12300
		d.add "xie",-12120
		d.add "xin",-12099
		d.add "xing",-12089
		d.add "xiong",-12074
		d.add "xiu",-12067
		d.add "xu",-12058
		d.add "xuan",-12039
		d.add "xue",-11867
		d.add "xun",-11861
		d.add "ya",-11847
		d.add "yan",-11831
		d.add "yang",-11798
		d.add "yao",-11781
		d.add "ye",-11604
		d.add "yi",-11589
		d.add "yin",-11536
		d.add "ying",-11358
		d.add "yo",-11340
		d.add "yong",-11339
		d.add "you",-11324
		d.add "yu",-11303
		d.add "yuan",-11097
		d.add "yue",-11077
		d.add "yun",-11067
		d.add "za",-11055
		d.add "zai",-11052
		d.add "zan",-11045
		d.add "zang",-11041
		d.add "zao",-11038
		d.add "ze",-11024
		d.add "zei",-11020
		d.add "zen",-11019
		d.add "zeng",-11018
		d.add "zha",-11014
		d.add "zhai",-10838
		d.add "zhan",-10832
		d.add "zhang",-10815
		d.add "zhao",-10800
		d.add "zhe",-10790
		d.add "zhen",-10780
		d.add "zheng",-10764
		d.add "zhi",-10587
		d.add "zhong",-10544
		d.add "zhou",-10533
		d.add "zhu",-10519
		d.add "zhua",-10331
		d.add "zhuai",-10329
		d.add "zhuan",-10328
		d.add "zhuang",-10322
		d.add "zhui",-10315
		d.add "zhun",-10309
		d.add "zhuo",-10307
		d.add "zi",-10296
		d.add "zong",-10281
		d.add "zou",-10274
		d.add "zu",-10270
		d.add "zuan",-10262
		d.add "zui",-10260
		d.add "zun",-10256
		d.add "zuo",-10254
	End Sub

	Private Function g(byval num)
		IF num>0 and num<160 Then
			g=chr(num)
		Else
		IF num<-20319 Or num>-10247 Then
			g=""
		Else
			Dim a,b
			Dim i
			a=d.Items
			b=d.keys
			For i=d.count-1 To 0 Step -1
				IF a(i)<=num Then Exit For
			Next
			g=b(i)
		End IF
		End IF
	End Function

	Private Function c(byval s)
		Dim p:p=s
		Dim temp:temp=""
		Dim i
		For i=1 To Len(p)
			temp = temp & g(Asc(Mid(p,i,1)))
		next
		c = temp
	End Function

End Class
%>