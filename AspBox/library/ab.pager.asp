<%
'######################################################################
'## ab.pager.asp
'## -------------------------------------------------------------------
'## Feature     :   AspBox Pager Class
'## Version     :   v1.0.1
'## Author      :   Lajox(lajox@19www.com)
'## Update Date :   2014/08/02 18:58
'## Description :   AspBox分页类(Pager)操作模块
'######################################################################

Class Cls_AB_Pager

	Private o_dict, o_regex, o_dom, o_pat, o_style, o_cache
	Private i_style, i_total, i_pagesize, i_pageno, i_pagecount, i_begin, i_end
	Private s_html, s_format, s_first, s_last, s_prev, s_next, s_list, s_select
	Private s_style, s_css, s_param, s_url
	Private b_rewrite, b_seturl

	Private Sub Class_Initialize()
		On Error Resume Next
		Set o_dict 		= Server.CreateObject(AB.dictName)
		Set o_pat 		= Server.CreateObject(AB.dictName)
		Set o_style		= Server.CreateObject(AB.dictName)
		s_format 		= "{first} {prev} {list} {next} {last}"
		s_style 		= ""
		s_param 		= "page"
		s_url 			= ""
		i_pagesize 		= 10
		i_style 		= 0
		b_rewrite 		= False
		b_seturl 		= False
		AB.Use "Cache" : Set o_cache = AB.Cache.New : o_cache.Expires = 30
		Init()
		On Error GoTo 0
	End Sub

	Private Sub Init()
		On Error Resume Next
		o_dict.add "first.display", "show"
		o_dict.add "first.class", "first"
		o_dict.add "first.dis-tag", "a"
		o_dict.add "first.text", "首 页"
		o_dict.add "prev.display", "show"
		o_dict.add "prev.class", "prev"
		o_dict.add "prev.dis-tag", "a"
		o_dict.add "prev.text", "上一页"
		o_dict.add "next.display", "show"
		o_dict.add "next.class", "next"
		o_dict.add "next.dis-tag", "a"
		o_dict.add "next.text", "下一页"
		o_dict.add "last.display", "show"
		o_dict.add "last.class", "last"
		o_dict.add "last.dis-tag", "a"
		o_dict.add "last.text", "尾 页"
		o_dict.add "list.display", "show"
		o_dict.add "list.link-text", "$n" 'e.g. '$n', '第$n页'
		o_dict.add "list.curr-tag", "a" '值:a|span; 缺省:a
		o_dict.add "list.curr-class", "current"
		o_dict.add "list.index", "4" '值:0|5#4|5
		o_dict.add "list.min", 0 '值:0|9
		o_dict.add "select.display", "show"
		o_dict.add "select.text", "第$n页"
		ProcessStyle(s_style)
		On Error GoTo 0
	End Sub

	Private Sub Class_Terminate()
		Set o_dict = Nothing
		Set o_pat = Nothing
		Set o_dom = Nothing
		Set o_style = Nothing
		Set o_cache = Nothing
	End Sub

	Public Function [New]()
		Set [New] = New Cls_AB_Pager
	End Function

	'——————————————————————————————————————————————————————————————————————————————————————————
	'# AB.Pager.Format 属性
	'# @syntax: AB.Pager.Format[ = str]
	'# @return: String (字符串) 查询此属性时，返回格式输出数据
	'# @dowhat: 此属性用于查询或者设置输出的格式话数据，默认值为“{first} {prev} {list} {next} {last}”。
	'——DESC————————————————————————————————————————————————————————————————————————————————————
	'# @param: str(可选) String (字符串) 设置格式输出数据
	'——DEMO————————————————————————————————————————————————————————————————————————————————————
	'# AB.Use "Pager"
	'# Dim pager : Set pager = AB.Pager.New
	'# 'pager.Css = "" '设置输出的Css样式
	'# pager.Format = "<div id=""pagerbar"">{first} {prev} {list} {next} {last}</div>" '设置格式输出数据
	'# 'pager.Style = "" '设置控制样式属性
	'# ''AB.C.PrintCn pager.FullStyle '用于调试查看完整的样式控制属性
	'# pager.Param = "page" '默认值: page
	'# 'pager.Url = "" '留空则缺省为当前页地址,例：pager.Url = "/test01/demo.asp?a=t&page={*}&f=gg#top"
	'# pager.Total = 11
	'# pager.PageSize = 3 '缺省值: 10
	'# AB.C.PrintCn "==========="
	'# AB.C.PrintCn "<strong>当前显示：第" & pager.Begin & "条 - 第" & pager.End & "条</strong>"
	'# AB.C.PrintCn "==========="
	'# AB.C.PrintCn "共" & pager.Total & "条数据 当前: 第" & pager.PageNo & "页/共" & pager.PageCount & "页 每页显示:" & pager.PageSize & "条"
	'# 'AB.C.Print pager.Html '用AB.Pager.Html获取最终HTML数据 或 用AB.Pager.Show()输出HTML数据
	'# pager.Show() '等同于 AB.C.Print pager.Html
	'——————————————————————————————————————————————————————————————————————————————————————————

	Public Property Let Format(ByVal s)
		s_format = s
	End Property
	Public Property Get Format()
		Format = s_format
	End Property

	'——————————————————————————————————————————————————————————————————————————————————————————
	'# AB.Pager.Style 属性
	'# @syntax: AB.Pager.Style[ = str]
	'# @alias: AB.Pager.addStyle str
	'# @return: String (字符串) 查询此属性时，返回样式控制属性
	'# @dowhat: 此属性用于查询或者设置样式控制属性，可用 AB.Pager.FullStyle 查看完整的样式控制属性。
	'——DESC————————————————————————————————————————————————————————————————————————————————————
	'# @param: str(可选) String (字符串) 设置控制样式属性
	'——DEMO————————————————————————————————————————————————————————————————————————————————————
	'# AB.Use "Pager"
	'# Dim pager : Set pager = AB.Pager.New
	'# pager.Format = "{first} {prev} {list} {next} {last}" '设置格式输出数据
	'# pager.Style = "list{display:hide}" '设置控制样式属性
	'# ''AB.C.PrintCn pager.FullStyle '用于调试查看
	'# pager.Param = "page" '默认值: page
	'# pager.Total = 15
	'# pager.PageSize = 10 '缺省值: 10
	'# pager.Show() '等同于 AB.C.Print pager.Html
	'——————————————————————————————————————————————————————————————————————————————————————————

	Public Property Let Style(ByVal s)
		i_style = AutoPlus(i_style)
		If Trim(s)<>"" Then
			o_style.add i_style, s
			ProcessStyle(s)
		End If
	End Property

	Public Sub addStyle(ByVal s)
		i_style = AutoPlus(i_style)
		If Trim(s)<>"" Then
			o_style.add i_style, s
			ProcessStyle(s)
		End If
	End Sub

	'——————————————————————————————————————————————————————————————————————————————————————————
	'# AB.Pager.FullStyle 属性
	'# @syntax: str = AB.Pager.FullStyle
	'# @return: String (字符串) 用于查看完整的样式控制属性
	'# @dowhat: 查看完整的样式控制属性(一般用于调试)。
	'# 样式说明：
	'# display:show|hide|auto; 控制显示状态：show(显示),hide(隐藏),auto(自动); 缺省值 show。
	'# class:first|prev|..; 设置CSS的class属性。
	'# id:first|prev|..; 设置CSS的id属性。
	'# dis-class:sinfo|..; 设置失去聚焦则显示的class属性，代替上面的class属性值。
	'# dis-tag:a|span|..; 设置失去聚焦则显示的标签; 缺省值a。
	'# dis-href:null|''|empty|js|js-1|js-2|..; 当dis-tag:a时，设置失去聚焦显示a标签的href属性。值为js等同于js-1,而值null|''|empty则君表示设置值为空。
	'# text:首页|上一页|..; 设置显示文本;
	'# wrap-tag:div|p|span|li|label|..; 设置外部包装标签(父标签); 缺省值为空。
	'# wrap-class:''; 设置外部包装标签的class属性;
	'# wrap-id:''; 设置外部包装标签的id属性;
	'# curr-tag:a|span|label|..; 设置当前页码链接的标签; 缺省值(a)。
	'# curr-class:''; 设置当前页码链接的标签的class属性;
	'# curr-id:''; 设置外部包装标签的id属性;
	'# link-text:'第$n页'; 设置分页条码链接的文字; 缺省值($n)。
	'# link-class:''; 设置分页条码链接的class属性;
	'# link-id:''; 设置分页条码链接的id属性;
	'# link-space:''; 设置分页条码各个链接的分隔符;
	'# dot-val:''; 设置当分页条码里后期还有链接的提示符(表示省略); 缺省值(..)
	'# dot-for:'a#xy'|'a#x'|'a#y'|'a'|'txt#xy'|'txt'|'span#xy'; 设置提示符包装标签和显示位置;
	'#     以#分隔开，前面部分设置包装标签，若值为txt则表示不设置标签
	'#     以#后面部分设置显示位置，值为xy或为空表示前后均显示，值为x表示只显示前面部分，值为y表示只显示后面部分
	'# index:0|'5#4'|5; 设置分页条码各个链接的分隔符; 缺省值(4).
	'#     以#分隔开，前面部分设置分页条码显示的当前页码向左延续显示的链接数目
	'#     以#后面部分设置分页条码显示的当前页码向右延续显示的链接数目，若无#号则表示左右延续显示的链接数目
	'# min:0|9|..; 设置分页条码最少链接数; 缺省值为0.
	'# curr-wrap-tag:a|span|label|..; 设置当前页码链接的包装标签(父标签); 缺省值(a)。
	'# curr-wrap-class:''; 设置当前页码链接的包装标签的class属性;
	'# curr-wrap-id:''; 设置当前页码链接的包装标签的id属性;
	'# link-wrap-tag:a|span|label|..; 设置页码每个链接的包装标签(父标签); 缺省值(a)。
	'# link-wrap-class:''; 设置页码每个链接的包装标签的class属性;
	'# link-wrap-id:''; 设置页码每个链接的包装标签的id属性;
	'# 下拉框特有属性：
	'# select{display:show|hide;name:'';text:'第$n页';class:'';id:'';curr-class:'current';curr-id:'';}
	'——DESC————————————————————————————————————————————————————————————————————————————————————
	'# @param: none
	'——DEMO————————————————————————————————————————————————————————————————————————————————————
	'# AB.C.PrintCn AB.Pager.FullStyle '用于调试查看完整的样式控制属性
	'# '_要实现下面分页数据html代码：
	'# ' <li class="first"><span>首 页</span></li> 
	'# ' <li class="prev"><span>上一页</span></li> 
	'# ' <li class="current"><span>1</span></li> 
	'# ' <li class="num"><a href="/t.asp?page=2">2</a></li> 
	'# ' <li class="num"><a href="/t.asp?page=3">3</a></li> 
	'# ' <li class="num"><a href="/t.asp?page=4">4</a></li> 
	'# ' <li class="next"><a href="/t.asp?page=2">下一页</a></li> 
	'# ' <li class="last"><a href="/t.asp?page=103">末 页</a></li>
	'# '_可用以下数据调用实现：
	'# AB.Pager.Format = "<div id=""pagelist""><ul>{first}\n {prev}\n {list}\n {next}\n {last}</ul></div>"
	'# AB.Pager.Style = ""
	'# AB.Pager.addStyle "first{display:show;class:'';dis-tag:span;text:首 页;wrap-tag:li;wrap-class:first;wrap-id:'';}"
	'# AB.Pager.addStyle "prev{display:auto;class:prev;dis-tag:span;text:上一页;wrap-tag:li;wrap-class:prev;wrap-id:'';}"
	'# AB.Pager.addStyle "next{display:auto;class:next;dis-tag:span;text:下一页;wrap-tag:li;wrap-class:next;wrap-id:'';}"
	'# AB.Pager.addStyle "last{display:show;class:last;dis-tag:span;text:尾 页;wrap-tag:li;wrap-class:last;wrap-id:'';}"
	'# 'AB.Pager.addStyle "list{dot-val:'..';dot-for:a#xy;}"
	'# AB.Pager.addStyle "list{curr-tag:span;curr-class:'';link-text:'$n';link-class:'';link-space:'\n';index:4|4;min:9;}"
	'# AB.Pager.addStyle "list{curr-wrap-tag:li;curr-wrap-class:'current';curr-wrap-id:'';}"
	'# AB.Pager.addStyle "list{link-wrap-tag:li;link-wrap-class:num;link-wrap-id:'';}"
	'# AB.Pager.Show
	'——————————————————————————————————————————————————————————————————————————————————————————

	Public Property Get FullStyle()
		FullStyle = GetFullStyle()
	End Property

	'——————————————————————————————————————————————————————————————————————————————————————————
	'# AB.Pager.Css 属性
	'# @syntax: AB.Pager.Css[ = str]
	'# @return: String (字符串) 分页样式的css字符串
	'# @dowhat: 用于设置(或获取自定义的)分页样式的css字符串。
	'——DESC————————————————————————————————————————————————————————————————————————————————————
	'# @param: str(可选) String (字符串) css字符串
	'——DEMO————————————————————————————————————————————————————————————————————————————————————
	'# Dim num
	'# 'num = Rs.RecordCount 'RS记录集总记录数
	'# num = 25
	'# AB.Use "Pager"
	'# Dim pager : Set pager = AB.Pager.New
	'# pager.Css = ".pagerbar{margin-bottom:10px;line-height:25px;float:right;}" &_
	'# 	".pagerbar a,.pagerbar span{color:#555;float:left;display:inline;margin-left:4px;padding:0 8px;border:1px solid #ccc;background:#fff;white-space:nowrap;}" &_
	'# 	".pagerbar a:link,.pagerbar a:visited{color:#ab0000;}" &_
	'# 	".pagerbar a:hover,.pagerbar a:active{border-color:#f49e48;text-decoration:none;}" &_
	'# 	".pagerbar .current,.pagerbar a.current:link,.pagerbar a.current:visited{font-weight:bold;color:#fff;border-color:#f49e48;background:#ffc469;}"
	'# pager.Format = "{first} {prev} {list} {next} {last}" '设置格式输出数据
	'# pager.Style = "list{display:hide}" '设置控制样式属性
	'# pager.addStyle "first{display:show;class:first;dis-tag:span;text:首 页;}" '控制样式的补充
	'# pager.addStyle "prev{display:auto;class:prev;dis-tag:span;text:上一页;}" '控制样式的补充
	'# pager.addStyle "next{display:auto;class:next;dis-tag:span;text:下一页;}" '控制样式的补充
	'# pager.addStyle "last{display:show;class:last;dis-tag:span;text:尾 页;}" '控制样式的补充
	'# 'AB.Trace pager.FullStyle '用于调试查看
	'# pager.Param = "page" '分页标识参数，默认值: page
	'# pager.Total = num '数据总记录数
	'# pager.PageSize = 10 '每页最大显示数，缺省值: 10
	'# pager.Show() '输出分页代码
	'——————————————————————————————————————————————————————————————————————————————————————————

	Public Property Let Css(ByVal s)
		s_css = s
	End Property
	Public Property Get Css()
		Css = s_css
	End Property

	'——————————————————————————————————————————————————————————————————————————————————————————
	'# AB.Pager.Param 属性
	'# @syntax: AB.Pager.Css[ = str]
	'# @return: String (字符串) 分页标识参数
	'# @dowhat: 用于设置(或获取自定义的)分页标识参数。
	'——DESC————————————————————————————————————————————————————————————————————————————————————
	'# @param: str(可选) String (字符串) 分页标识参数
	'——DEMO————————————————————————————————————————————————————————————————————————————————————
	'# AB.Pager.Param = "p" '分页标识参数，默认值: page
	'——————————————————————————————————————————————————————————————————————————————————————————

	Public Property Let Param(ByVal s)
		If IsNull(s) Or Trim(s) = "" Then s = "page"
		s_param = s
	End Property
	Public Property Get Param()
		If IsNull(s_param) Or Trim(s_param) = "" Then s_param = "page"
		Param = s_param
	End Property

	'——————————————————————————————————————————————————————————————————————————————————————————
	'# AB.Pager.Url 属性
	'# @syntax: AB.Pager.Url[ = surl]
	'# @return: String (字符串) 分页链接地址形式
	'# @dowhat: 设置分页链接地址形式 或 获取当前页分页链接地址(自动补齐分页标识参数)。
	'——DESC————————————————————————————————————————————————————————————————————————————————————
	'# @param: surl(可选) String (字符串) 分页链接地址形式
	'#   特殊含义字符：
	'#    {*} 表示 当前替换当前页码
	'#    {$url} 表示 当前页面相对URL
	'#    {$path} 表示 当前页面文档相对URL路径
	'#    {$fullurl} 表示 当前页面绝对URL
	'#    {$fullpath} 表示 当前页面文档绝对URL路径
	'#    {$page} 表示 当前页面文档名称
	'#    {$param} 表示 当前页面地址参数
	'#    {$pageparam} 表示 当前页面文档名称+地址参数
	'#    除上面之外，如 {id} 还表示 当前URL的参数id，{name} 表示 当前URL的参数name ...
	'——DEMO————————————————————————————————————————————————————————————————————————————————————
	'# AB.Pager.Url = "" '分页链接地址设置，默认值为空，自动读取当前页url并自动补齐分页标识参数
	'# '_动态地址设置，e.g.
	'# 'AB.Pager.Url = "/news/list.asp?fk=show" '分页将自动补齐地址形式为：/news/list.asp?fk=show&page=2
	'# 'AB.Pager.Url = "/news/list.asp?fk=show&page={*}" '分页地址将{*}替换为页码：/news/list.asp?fk=show&page=2
	'# 'AB.Pager.Url = "/news/list.asp?fk={fk}&page={*}" '分页地址将{fk}替换为页面地址参数fk：/news/list.asp?fk=show&page=2
	'# '_伪静态设置，e.g.
	'# ' 假设伪静态地址：/demoa/demo/rewrite/demo02-1-1.html ，而实际url访问地址：/demoa/demo/rewrite/demo02.asp?id=1&page=1
	'# ' 假设设置好了以下伪静态规则：
	'# ' # RewriteRule ^(.*)/demo02-([0-9]+)-([0-9]+)\.html$ $1/demo02\.asp\?id=$2&page=$3
	'# ' # RewriteRule ^(.*)/demo02-([0-9]+)\.html$ $1/demo02\.asp\?id=$2&page=1
	'# AB.Pager.Rewrite = True '是否开启伪静态地址, 缺省值 False
	'# 'AB.Pager.Url = "demo02-{id}-{*}.html" '第一页和第n页设置形式统一
	'# '_单独设置第n页和第一页地址形式（用{::}分隔开，前部分表示第n页设置形式，后部分表示第1页设置形式）:
	'# AB.Pager.Url = "demo02-{id}-{*}.html{::}demo02-{id}.html"
	'——————————————————————————————————————————————————————————————————————————————————————————

	Public Property Let Url(ByVal s)
		s_url = s
		b_seturl = True
	End Property
	Public Property Get Url()
		's_url = GetUrl()
		If b_seturl Then
			s_url = GetUrl()
			o_cache("ab_page:url") = s_url
			o_cache("ab_page:url").SaveApp
			b_seturl = False
		Else
			If o_cache("ab_page:url").Ready Then
				s_url = o_cache("ab_page:url").Value
			Else
				s_url = GetUrl()
				o_cache("ab_page:url") = s_url
				o_cache("ab_page:url").SaveApp
			End If
		End If
		Url = s_url
	End Property

	'——————————————————————————————————————————————————————————————————————————————————————————
	'# AB.Pager.Rewrite 属性
	'# @syntax: AB.Pager.Rewrite[ = bool]
	'# @return: Boolean (布尔值) 是否开启伪静态地址
	'# @dowhat: 用于设置是否开启伪静态地址。
	'——DESC————————————————————————————————————————————————————————————————————————————————————
	'# @param: bool(可选) Boolean (布尔值)
	'——DEMO————————————————————————————————————————————————————————————————————————————————————
	'# AB.Pager.Rewrite = True '是否开启伪静态地址，缺省值: False
	'——————————————————————————————————————————————————————————————————————————————————————————

	Public Property Let Rewrite(ByVal s)
		If Not (IsNull(s) and Trim(s)="") Then
			Dim bool : bool = CBool(s)
			b_rewrite = bool
		Else
			b_rewrite = False
		End If
	End Property
	Public Property Get Rewrite()
		Rewrite = b_rewrite
	End Property

	'-- pager属性

	Public Property Let Dom(ByVal o)
		If IsObject(o) Then Set o_dom = o Else o_dom = o
		If IsObject(o) Then
			If TypeName(o) = "Dictionary" Then '字典对象
				i_total = o.Count
			ElseIf TypeName(o) = "Recordset" Then 'rs对象
				If o.State = 0 Then '对象已关闭
					i_total = 0
				Else
					AB.Use "db"
					If Not AB.C.IsNul(o) Then
						'i_total = o.RecordCount
						i_total = AB.db.RsCount(o)
					Else
						i_total = 0
					End If
				End if
			End If
		ElseIf IsArray(o) Then '数组
			AB.Use "A"
			If AB.A.Len(o)>0 Then i_total = AB.A.Len(o) Else i_total = 0
		End If
		If i_total<0 Then i_total = 0
	End Property

	'——————————————————————————————————————————————————————
	'# AB.Pager.Total 属性
	'# @syntax: AB.Pager.Total[ = num]
	'# @dowhat: 用于设置数据总记录数
	'——————————————————————————————————————————————————————

	Public Property Let Total(ByVal n)
		i_total = n
	End Property
	Public Property Get Total()
		Total = i_total
	End Property

	'——————————————————————————————————————————————————————
	'# AB.Pager.PageSize 属性
	'# @syntax: AB.Pager.PageSize[ = num]
	'# @dowhat: 用于设置分页每页最大显示数
	'——————————————————————————————————————————————————————

	Public Property Let PageSize(ByVal n)
		If Not IsNumeric(n) Then n = 0
		n = CLng(n)
		If n <=0 Then n = 10
		i_pagesize = n
	End Property
	Public Property Get PageSize()
		PageSize = i_pagesize
	End Property

	'——————————————————————————————————————————————————————
	'# AB.Pager.PageNo 属性
	'# @syntax: n = AB.Pager.PageNo
	'# @dowhat: 获取当前页码
	'——————————————————————————————————————————————————————

	Public Property Get PageNo()
		i_pageno = GetPageNo()
		PageNo = i_pageno
	End Property

	'——————————————————————————————————————————————————————
	'# AB.Pager.PageCount 属性
	'# @syntax: n = AB.Pager.PageCount
	'# @dowhat: 获取分页页面总数
	'——————————————————————————————————————————————————————

	Public Property Get PageCount()
		i_pagecount = GetPageCount()
		PageCount = i_pagecount
	End Property

	'-- 输出数据

	'——————————————————————————————————————————————————————————————————————————————————————————
	'# AB.Pager.Begin 属性
	'# @syntax: n = AB.Pager.Begin
	'# @dowhat: 获取当前页面开始数据（从第几条开始）
	'——DEMO————————————————————————————————————————————————————————————————————————————————————
	'# AB.Pager.Rewrite = True '是否开启伪静态地址，缺省值: False
	'# AB.Use "Pager"
	'# Dim pager : Set pager = AB.Lib("Pager")
	'# pager.Format = "<div id=""pagerbar"">{first} {prev} {list} {next} {last}</div>" '设置格式输出数据
	'# pager.Total = 11
	'# pager.PageSize = 3 '缺省值: 10
	'# AB.C.PrintCn "==========="
	'# AB.C.PrintCn "<strong>当前显示：第" & pager.Begin & "条 - 第" & pager.End & "条</strong>"
	'# AB.C.PrintCn "==========="
	'# AB.C.PrintCn "共" & pager.Total & "条数据 当前: 第" & pager.PageNo & "页/共" & pager.PageCount & "页 每页显示:" & pager.PageSize & "条"
	'# pager.Show()
	'——————————————————————————————————————————————————————————————————————————————————————————

	Public Property Get [Begin]()
		i_begin = GetRange(0)
		[Begin] = i_begin
	End Property

	'——————————————————————————————————————————————————————
	'# AB.Pager.End 属性
	'# @syntax: n = AB.Pager.End
	'# @dowhat: 获取当前页面结束数据（到第几条结束）
	'——————————————————————————————————————————————————————

	Public Property Get [End]()
		i_end = GetRange(1)
		[End] = i_end
	End Property

	'——————————————————————————————————————————————————————
	'# AB.Pager.First 属性
	'# @syntax: n = AB.Pager.First
	'# @dowhat: 获取{首页}(html代码)
	'——————————————————————————————————————————————————————

	Public Property Get First()
		Dim n : n = Me.PageNo
		Dim s1, s2, p : p = 1
		Dim s_class, s_id, s_distag, s_dishref, href
		If o_dict.Exists("first.dis-tag") And Trim(o_dict("first.dis-tag"))<>"" Then s_distag = LCase(Trim(o_dict("first.dis-tag")))
		If o_dict.Exists("first.dis-href") And Trim(o_dict("first.dis-href"))<>"" Then s_dishref = LCase(Trim(o_dict("first.dis-href")))
		If Trim(s_distag) = "" Then s_distag = "a"
		If Lcase(s_dishref) = "js" Or Lcase(s_dishref) = "js-1" Then s_dishref = "javascript:;"
		If Lcase(s_dishref) = "js-2" Then s_dishref = "javascript:void(0);"
		If o_dict.Exists("first.dis-href") Then
			If n-1 <= 0 Then href = s_dishref Else href = LinkUrl(p)
		Else
			href = LinkUrl(p)
		End If
		's_class = GetCssClass("first.class") : s_id = GetCssId("first.id")
		If o_dict.Exists("first.dis-class") And Trim(o_dict("first.dis-class"))<>"" Then
			s_class = GetCssClass("first.dis-class")
		Else
			s_class = GetCssClass("first.class")
		End If
		If o_dict.Exists("first.dis-id") And Trim(o_dict("first.dis-id"))<>"" Then
			s_id = GetCssId("first.dis-id")
		Else
			s_id = GetCssId("first.id")
		End If
		s1 = "<a href=""" & href & """" & s_class & s_id & ">" & o_dict.item("first.text") & "<"&"/a>"
		s2 = "<"&s_distag & "" & s_class & s_id & ">" & o_dict.item("first.text") & "<"&"/"&s_distag&">"
		If s_distag = "a" Then
			s_first = s1
		Else
			If n-1 <= 0 Then s_first = s2 Else s_first = s1
		End If
		If o_dict.Exists("first.wrap-tag") Then
			Dim s, t
			p = Trim(o_dict("first.wrap-tag")) : s = "" : t = ""
			If o_dict.Exists("first.wrap-class") And Trim(o_dict("first.wrap-class"))<>"" Then s=" class="""&Trim(o_dict("first.wrap-class"))&""""
			If o_dict.Exists("first.wrap-id") And Trim(o_dict("first.wrap-id"))<>"" Then t=" id="""&Trim(o_dict("first.wrap-id"))&""""
			If p <> "" Then s_first = "<"&p&s&t&">" & s_first & "</"&p&">"
		End If
		If Me.Total<=0 Then s_first = "" '空记录
		First = s_first
	End Property

	'——————————————————————————————————————————————————————
	'# AB.Pager.Prev 属性
	'# @syntax: n = AB.Pager.Prev
	'# @dowhat: 获取{上一页}(html代码)
	'——————————————————————————————————————————————————————

	Public Property Get Prev()
		Dim n : n = Me.PageNo
		Dim s1, s2, p : p = n - 1 : If p < 1 Then p = 1
		Dim s_class, s_id, s_distag, s_dishref, href
		If o_dict.Exists("prev.dis-tag") And Trim(o_dict("prev.dis-tag"))<>"" Then s_distag = LCase(Trim(o_dict("prev.dis-tag")))
		If o_dict.Exists("prev.dis-href") And Trim(o_dict("prev.dis-href"))<>"" Then s_dishref = LCase(Trim(o_dict("prev.dis-href")))
		If Trim(s_distag) = "" Then s_distag = "a"
		If Lcase(s_dishref) = "js" Or Lcase(s_dishref) = "js-1" Then s_dishref = "javascript:;"
		If Lcase(s_dishref) = "js-2" Then s_dishref = "javascript:void(0);"
		If o_dict.Exists("prev.dis-href") Then
			If n-1 <= 0 Then href = s_dishref Else href = LinkUrl(p)
		Else
			href = LinkUrl(p)
		End If
		's_class = GetCssClass("prev.class") : s_id = GetCssId("prev.id")
		If o_dict.Exists("prev.dis-class") And Trim(o_dict("prev.dis-class"))<>"" Then
			s_class = GetCssClass("prev.dis-class")
		Else
			s_class = GetCssClass("prev.class")
		End If
		If o_dict.Exists("prev.dis-id") And Trim(o_dict("prev.dis-id"))<>"" Then
			s_id = GetCssId("prev.dis-id")
		Else
			s_id = GetCssId("prev.id")
		End If
		s1 = "<a href=""" & href & """" & s_class & s_id & ">" & o_dict.item("prev.text") & "<"&"/a>"
		s2 = "<"&s_distag & "" & s_class & s_id & ">" & o_dict.item("prev.text") & "<"&"/"&s_distag&">"
		If s_distag = "a" Then s_prev = s1 Else s_prev = s2
		If s_distag = "a" Then
			s_prev = s1
		Else
			If n-1 <= 0 Then s_prev = s2 Else s_prev = s1
		End If
		If o_dict.Exists("prev.wrap-tag") Then
			Dim s, t
			p = Trim(o_dict("prev.wrap-tag")) : s = "" : t = ""
			If o_dict.Exists("prev.wrap-class") And Trim(o_dict("prev.wrap-class"))<>"" Then s=" class="""&Trim(o_dict("prev.wrap-class"))&""""
			If o_dict.Exists("prev.wrap-id") And Trim(o_dict("prev.wrap-id"))<>"" Then t=" id="""&Trim(o_dict("prev.wrap-id"))&""""
			If p <> "" Then s_prev = "<"&p&s&t&">" & s_prev & "</"&p&">"
		End If
		If Me.Total<=0 Then s_prev = "" '空记录
		Prev = s_prev
	End Property

	'——————————————————————————————————————————————————————
	'# AB.Pager.Next 属性
	'# @syntax: n = AB.Pager.Next
	'# @dowhat: 获取{下一页}(html代码)
	'——————————————————————————————————————————————————————

	Public Property Get [Next]()
		Dim n, m : n = Me.PageNo : m = Me.PageCount
		Dim s1, s2, p : p = n + 1 : If p > m Then p = m
		Dim s_class, s_id, s_distag, s_dishref, href
		If o_dict.Exists("next.dis-tag") And Trim(o_dict("next.dis-tag"))<>"" Then s_distag = LCase(Trim(o_dict("next.dis-tag")))
		If o_dict.Exists("next.dis-href") And Trim(o_dict("next.dis-href"))<>"" Then s_dishref = LCase(Trim(o_dict("next.dis-href")))
		If Trim(s_distag) = "" Then s_distag = "a"
		If Lcase(s_dishref) = "js" Or Lcase(s_dishref) = "js-1" Then s_dishref = "javascript:;"
		If Lcase(s_dishref) = "js-2" Then s_dishref = "javascript:void(0);"
		If o_dict.Exists("next.dis-href") Then
			If n+1-m > 0 Then href = s_dishref Else href = LinkUrl(p)
		Else
			href = LinkUrl(p)
		End If
		's_class = GetCssClass("next.class") : s_id = GetCssId("next.id")
		If o_dict.Exists("next.dis-class") And Trim(o_dict("next.dis-class"))<>"" Then
			s_class = GetCssClass("next.dis-class")
		Else
			s_class = GetCssClass("next.class")
		End If
		If o_dict.Exists("next.dis-id") And Trim(o_dict("next.dis-id"))<>"" Then
			s_id = GetCssId("next.dis-id")
		Else
			s_id = GetCssId("next.id")
		End If
		s1 = "<a href=""" & href & """" & s_class & s_id & ">" & o_dict.item("next.text") & "<"&"/a>"
		s2 = "<"&s_distag & "" & s_class & s_id & ">" & o_dict.item("next.text") & "<"&"/"&s_distag&">"
		If s_distag = "a" Then
			s_next = s1
		Else
			If n+1-m > 0 Then s_next = s2 Else s_next = s1
		End If
		If o_dict.Exists("next.wrap-tag") Then
			Dim s, t
			p = Trim(o_dict("next.wrap-tag")) : s = "" : t = ""
			If o_dict.Exists("next.wrap-class") And Trim(o_dict("next.wrap-class"))<>"" Then s=" class="""&Trim(o_dict("next.wrap-class"))&""""
			If o_dict.Exists("next.wrap-id") And Trim(o_dict("next.wrap-id"))<>"" Then t=" id="""&Trim(o_dict("next.wrap-id"))&""""
			If p <> "" Then s_next = "<"&p&s&t&">" & s_next & "</"&p&">"
		End If
		If Me.Total<=0 Then s_next = "" '空记录
		[Next] = s_next
	End Property

	'——————————————————————————————————————————————————————
	'# AB.Pager.Last 属性
	'# @syntax: n = AB.Pager.Last
	'# @dowhat: 获取{尾页}(html代码)
	'——————————————————————————————————————————————————————

	Public Property Get Last()
		Dim n, m : n = Me.PageNo : m = Me.PageCount
		Dim s1, s2, p : p = m
		Dim s_class, s_id, s_distag, s_dishref, href
		If o_dict.Exists("last.dis-tag") And Trim(o_dict("last.dis-tag"))<>"" Then s_distag = LCase(Trim(o_dict("last.dis-tag")))
		If o_dict.Exists("last.dis-href") And Trim(o_dict("last.dis-href"))<>"" Then s_dishref = LCase(Trim(o_dict("last.dis-href")))
		If Trim(s_distag) = "" Then s_distag = "a"
		If Lcase(s_dishref) = "js" Or Lcase(s_dishref) = "js-1" Then s_dishref = "javascript:;"
		If Lcase(s_dishref) = "js-2" Then s_dishref = "javascript:void(0);"
		If o_dict.Exists("last.dis-href") Then
			If n+1-m > 0 Then href = s_dishref Else href = LinkUrl(p)
		Else
			href = LinkUrl(p)
		End If
		's_class = GetCssClass("last.class") : s_id = GetCssId("last.id")
		If o_dict.Exists("last.dis-class") And Trim(o_dict("last.dis-class"))<>"" Then
			s_class = GetCssClass("last.dis-class")
		Else
			s_class = GetCssClass("last.class")
		End If
		If o_dict.Exists("last.dis-id") And Trim(o_dict("last.dis-id"))<>"" Then
			s_id = GetCssId("last.dis-id")
		Else
			s_id = GetCssId("last.id")
		End If
		s1 = "<a href=""" & href & """" & s_class & s_id & ">" & o_dict.item("last.text") & "<"&"/a>"
		s2 = "<"&s_distag & "" & s_class & s_id & ">" & o_dict.item("last.text") & "<"&"/"&s_distag&">"
		If s_distag = "a" Then
			s_last = s1
		Else
			If n+1-m > 0 Then s_last = s2 Else s_last = s1
		End If
		If o_dict.Exists("last.wrap-tag") Then
			Dim s, t
			p = Trim(o_dict("last.wrap-tag")) : s = "" : t = ""
			If o_dict.Exists("last.wrap-class") And Trim(o_dict("last.wrap-class"))<>"" Then s=" class="""&Trim(o_dict("last.wrap-class"))&""""
			If o_dict.Exists("last.wrap-id") And Trim(o_dict("last.wrap-id"))<>"" Then t=" id="""&Trim(o_dict("last.wrap-id"))&""""
			If p <> "" Then s_last = "<"&p&s&t&">" & s_last & "</"&p&">"
		End If
		If Me.Total<=0 Then s_last = "" '空记录
		Last = s_last
	End Property

	'——————————————————————————————————————————————————————
	'# AB.Pager.List 属性
	'# @syntax: n = AB.Pager.List
	'# @dowhat: 获取分页条码{html代码}
	'——————————————————————————————————————————————————————

	Public Property Get List()
		If Me.Total<=0 Then
			s_list = "" '空记录
		Else
			s_list = GetList()
		End If
		List = s_list
	End Property

	'——————————————————————————————————————————————————————
	'# AB.Pager.Select 属性
	'# @syntax: n = AB.Pager.Select
	'# @dowhat: 获取分页数据的下拉选框代码
	'——————————————————————————————————————————————————————

	Public Property Get [Select]()
		s_select = GetSelect()
		[Select] = s_select
	End Property

	'——————————————————————————————————————————————————————
	'# AB.Pager.Html 属性
	'# @syntax: n = AB.Pager.Html
	'# @dowhat: 获取分页最终输出数据的html代码
	'——————————————————————————————————————————————————————

	Public Function Html()
		Dim s1, s2
		If Not (IsNull(s_css) Or Trim(s_css) = "") Then s1 = "<style type=""text/css"">" & s_css & "<"&"/style>"
		s2 = GetFormatHtml()
		s_html = s1 & s2
		Html = s_html
	End Function

	'——————————————————————————————————————————————————————
	'# AB.Pager.Show 属性
	'# @syntax: n = AB.Pager.Show
	'# @dowhat: 输出分页最终数据的html代码
	'——————————————————————————————————————————————————————

	Public Sub Show()
		AB.C.Print Me.Html
	End Sub

	Public Property Get Data()
		Set Data = o_dict
	End Property

	' ============== 以下辅助函数 ===================

	Private Function GetPageNo()
		Dim p : p = Request.QueryString(s_param)
		If Not IsNumeric(p) Then p = 1
		p = CLng(p)
		If p<=0 Then p = 1
		GetPageNo = p
	End Function

	Private Function GetPageCount()
		If i_total <=0 Then : GetPageCount = 0 : Exit Function : End If
		Dim m, n : m = (i_total Mod i_pagesize) : n = (i_total \ i_pagesize)
		Dim p : p = 1
		If m = 0 Then p = n Else p = n + 1
		GetPageCount = p
	End Function

	Private Function GetRange(ByVal n)
		Dim pSize, pNo, pMax : pSize = Me.PageSize : pNo = Me.PageNo : pMax = Me.PageCount
		Dim nTotal : nTotal = Me.Total
		Dim min, max, a, temp
		If (pNo * pSize) <= nTotal Then
			min = (pNo - 1) * pSize + 1
			max = pNo * pSize
		Else
			min = (pMax - 1) * pSize + 1
			max = nTotal
		End If
		If min<0 Then min=0
		If max<0 Then max=0
		a = Array(min, max)
		If n = 0 Then temp = a(0)
		If n = 1 Then temp = a(1)
		GetRange = temp
	End Function

	Private Function GetFormatHtml()
		Dim s : s = ""
		Dim t_first, t_prev, t_list, t_next, t_last, t_select
		t_first = Me.First : t_prev = Me.Prev : t_next = Me.Next : t_last = Me.Last : t_list = Me.List
		If Instr(s_format,"{select}")>0 And o_dict.Exists("select.display") And Trim(o_dict("select.display")) <> "hide" Then t_select = Me.Select
		Dim pNo, pMax : pNo = Me.PageNo : pMax = Me.PageCount()
		Dim pX, pY : pX = pNo - 1 : pY = pNo + 1
		If Trim(o_dict("first.display")) = "hide" Then t_first = ""
		If Trim(o_dict("prev.display")) = "hide" Then t_prev = ""
		If Trim(o_dict("list.display")) = "hide" Then t_list = ""
		If Trim(o_dict("next.display")) = "hide" Then t_next = ""
		If Trim(o_dict("last.display")) = "hide" Then t_last = ""
		If Trim(o_dict("select.display")) = "hide" Then t_select = ""
		If Trim(o_dict("first.display")) = "auto" Then If pMax<=1 Or pNo = 1 Then t_first = ""
		If Trim(o_dict("last.display")) = "auto" Then If pMax<=1 Or pNo>=pMax Then t_last = ""
		If Trim(o_dict("prev.display")) = "auto" Then If pMax<=1 Or pX<1 Then t_prev = ""
		If Trim(o_dict("next.display")) = "auto" Then If pMax<=1 Or pY>pMax Then t_next = ""
		s = AB.C.RP(s_format, Array("\\","\n",Chr(31)), Array("\",VBCrlf,Chr(31)))
		s = AB.C.RP(s, Array("{first}", "{prev}", "{list}", "{next}", "{last}", "{select}"), Array(t_first, t_prev, t_list, t_next, t_last, t_select))
		GetFormatHtml = s
	End Function

	Private Function GetPageFullURL()
		GetPageFullURL = AB.C.GetUrl("")
	End Function

	Private Function GetPageURL()
		GetPageURL = AB.C.GetUrl(1)
	End Function

	Private Function GetPagePath()
		GetPagePath = AB.C.GetUrl(2)
	End Function

	Private Function GetPageFullPath()
		Dim host : host = AB.C.GetUrl("-1")
		GetPageFullPath = host & AB.C.GetUrl(2)
	End Function

	Private Function GetPageName()
		GetPageName = AB.C.GetUrl(5)
	End Function

	Private Function GetPageParam()
		GetPageParam = AB.C.GetUrl(6)
	End Function

	Private Function GetPageNameParam()
		GetPageNameParam = AB.C.GetUrl(4)
	End Function

	Private Function GetUrl()
		Dim u, ux, uy : u = Trim(s_url)
		If IsNull(u) Or Trim(u) = "" Or Trim(u) = "{$url}" Then u = GetPageURL()
		If b_rewrite Then : GetUrl = u : Exit Function : End If
		If Instr(u, "{::}")>0 Then
			ux = Trim(AB.C.CLeft(u, "{::}")) : uy = Trim(AB.C.CRight(u, "{::}"))
		Else
			ux = u : uy = ""
		End If
		If Instr(ux,"'")>0 Or Instr(ux,"""")>0 Then ux = AB.C.RegReplace(ux, "^(['""])(.*?)\1$", "$2")
		If Instr(ux,"{$url}")>0 Then ux = Replace(ux, "{$url}", GetPageURL())
		If Instr(uy,"{$url}")>0 Then uy = Replace(uy, "{$url}", GetPageURL())
		If Instr(ux,"{$path}")>0 Then ux = Replace(ux, "{$path}", GetPagePath())
		If Instr(uy,"{$path}")>0 Then uy = Replace(uy, "{$path}", GetPagePath())
		If Instr(ux,"{$fullurl}")>0 Then ux = Replace(ux, "{$fullurl}", GetPageFullURL())
		If Instr(uy,"{$fullurl}")>0 Then uy = Replace(uy, "{$fullurl}", GetPageFullURL())
		If Instr(ux,"{$fullpath}")>0 Then ux = Replace(ux, "{$fullpath}", GetPageFullPath())
		If Instr(uy,"{$fullpath}")>0 Then uy = Replace(uy, "{$fullpath}", GetPageFullPath())
		If Instr(ux,"{$page}")>0 Then ux = Replace(ux, "{$page}", GetPageName())
		If Instr(uy,"{$page}")>0 Then uy = Replace(uy, "{$page}", GetPageName())
		If Instr(ux,"{$param}")>0 Then ux = Replace(ux, "{$param}", GetPageParam())
		If Instr(uy,"{$param}")>0 Then uy = Replace(uy, "{$param}", GetPageParam())
		If Instr(ux,"{$pageparam}")>0 Then ux = Replace(ux, "{$pageparam}", GetPageNameParam())
		If Instr(uy,"{$pageparam}")>0 Then uy = Replace(uy, "{$pageparam}", GetPageNameParam())
		Dim u_a, u_b : u_a = AB.C.CLeft(ux, "?") : u_b = AB.C.CRight(ux, "?")
		Dim temp, a, i, k, s_x, s_y, stmp
		If Instr(u_b, "&")>0 Then
			o_pat.RemoveAll()
			a = Split(u_b, "&")
			For i=0 To UBound(a)
				s_x = Trim(AB.C.CLeft(a(i), "="))
				s_y = AB.C.CRight(a(i), "=")
				If s_x <> "" Then
					If Not o_pat.Exists(s_x) Then
						o_pat.add s_x, s_y
					Else
						o_pat(s_x) = o_pat.item(s_x) & ", " & s_y
					End IF
				End If
			Next
		Else
			s_x = Trim(AB.C.CLeft(u_b, "="))
			s_y = AB.C.CRight(u_b, "=")
			If s_x <> "" Then
				If Not o_pat.Exists(s_x) Then
					o_pat.add s_x, s_y
				Else
					o_pat(s_x) = o_pat.item(s_x) & ", " & s_y
				End IF
			End If
		End If
		If Not o_pat.Exists(s_param) Then o_pat.add s_param, "{*}" Else o_pat(s_param) = "{*}"
		k = 0
		For Each i In o_pat
			If k > 0 Then
				stmp = stmp & "&" & i & "=" & o_pat(i)
			Else
				stmp = i & "=" & o_pat(i)
			End If
			k = k + 1
		Next
		temp = u_a & "?" & stmp
		If uy <> "" Then temp = temp & "{::}" & uy
		GetUrl = temp
	End Function

	Private Function LinkUrl(ByVal n)
		Dim s, u : u = Me.Url() : s = u
		If Instr(s, "{::}")>0 Then
			If n = 1 Then
				Dim tmp : tmp = Trim(AB.C.CRight(s, "{::}"))
				If tmp <> "" Then s = tmp Else s = Trim(AB.C.CLeft(s, "{::}"))
			Else
				s = Trim(AB.C.CLeft(s, "{::}"))
			End If
		End If
		s = Replace(s, s_param&"={*}", s_param&"="&cstr(n))
		s = Replace(s, "{*}", cstr(n))
		'If b_rewrite Then
			Dim Matches, Match, rule, q
			rule = "\{(\w+)\}"
			Set Matches = AB.C.RegMatch(s, rule)
			For Each Match In Matches
				q = Trim(LCase(Match.SubMatches(0)))
				s = Replace(s, Match.Value, Request.QueryString(q))
			Next
		'End If
		LinkUrl = s
	End Function

	Private Function GetCssClass(ByVal k)
		Dim s
		If o_dict.Exists(k) And Trim(o_dict(k)) <> "" Then s = " class=""" & Trim(o_dict(k)) & """"
		GetCssClass = s
	End Function

	Private Function GetCssId(ByVal k)
		Dim s
		If o_dict.Exists(k) And Trim(o_dict(k)) <> "" Then s = " id=""" & Trim(o_dict(k)) & """"
		GetCssId = s
	End Function

	Private Function TmdClass(ByVal k)
		Dim s : If Trim(k)<>"" Then s=" class="""&Trim(k)&""""
		TmdClass = s
	End Function

	Private Function TmdId(ByVal k)
		Dim s : If Trim(k)<>"" Then s=" id="""&Trim(k)&""""
		TmdId = s
	End Function

	Private Function GetLinkText(ByVal p, ByVal n)
		Dim s : s = Replace(p,"$n",n)
		GetLinkText = s
	End Function

	Private Function AutoPlus(ByVal n)
		On Error Resume Next
		If Not IsNumeric(n) Then : AutoPlus = 1 : Exit Function : End If
		n = CLng(n)
		If n < 0 Then n = 0
		n = n + 1
		AutoPlus = n
		On Error Goto 0
	End Function

	Private Function GetFullStyle()
		Dim i, s1, s2, s3, s4, s5, temp, wrap
		For Each i In o_dict
			If AB.C.RegTest(i, "^(first|prev|list|next|last)\.") Then
				wrap = Trim(AB.C.CLeft(i,"."))
				key = Trim(AB.C.CRight(i,"."))
				If key<>"" Then
					If Trim(AB.C.CLeft(i,"."))="first" Then s1 = s1 & key & ":" & Trim(o_dict(i)) & ";"
					If Trim(AB.C.CLeft(i,"."))="prev" Then s2 = s2 & key & ":" & Trim(o_dict(i)) & ";"
					If Trim(AB.C.CLeft(i,"."))="list" Then s3 = s3 & key & ":" & Trim(o_dict(i)) & ";"
					If Trim(AB.C.CLeft(i,"."))="next" Then s4 = s4 & key & ":" & Trim(o_dict(i)) & ";"
					If Trim(AB.C.CLeft(i,"."))="last" Then s5 = s5 & key & ":" & Trim(o_dict(i)) & ";"
				End If
			End If
		Next
		s1 = "first{" & s1 & "}"
		s2 = "prev{" & s2 & "}"
		s3 = "list{" & s3 & "}"
		s4 = "next{" & s4 & "}"
		s5 = "last{" & s5 & "}"
		temp = s1 & s2 & s3 & s4 & s5
		GetFullStyle = temp
	End Function

	Private Function FK(ByVal v)
		Dim s : s = v : s = AB.C.RegReplace(s, "^((['""])(.*?)\2)$", "$3") : FK = s
	End Function

	Private Function GetList()
		If o_cache("ab_page:list").Ready and o_cache("ab_page:dict").Ready Then
			If IsObject(o_cache("ab_page:dict").Value) Then
				If o_dict Is o_cache("ab_page:dict").Value Then
					If o_cache("ab_page:list").Value <> "" Then
						GetList = o_cache("ab_page:list").Value
						Exit Function
					End If
				End If
			End If
		End If
		Dim temp, p, s, t, p1, p2, p3, ls : ls = "list"
		Dim n, pNo, pMax : pNo = Me.PageNo : pMax = Me.PageCount
		Dim ltxt : ltxt = FK(o_dict.item(ls&".link-text"))
		Dim a_class, a_id, b_class, b_id
		Dim curr_wrap_tag, curr_wrap_class, curr_wrap_id, link_wrap_tag, link_wrap_class, link_wrap_id
		Dim pa, pb : pa = 0 : pb = 0
		Dim curtag : If o_dict.Exists(ls&".curr-tag") Then curtag = LCase(Trim(o_dict(ls&".curr-tag"))) Else curtag = "a"
		Dim space : If o_dict.Exists(ls&".link-space") Then space = o_dict(ls&".link-space")
		space = AB.C.RP(space, Array("\\","\n",Chr(31)), Array("\",VBCrlf,Chr(31)))
		Dim dotval : If o_dict.Exists(ls&".dot-val") And Trim(o_dict(ls&".dot-val"))<>"" Then dotval = Trim(o_dict(ls&".dot-val"))
		Dim dotfor : If o_dict.Exists(ls&".dot-for") And Trim(o_dict(ls&".dot-for"))<>"" Then dotfor = Trim(o_dict(ls&".dot-for"))
		Dim dftag, dfval : dftag = LCase(Trim(AB.C.CLeft(dotfor, "#"))) : If InStr(dotfor,"#")>0 Then dfval = Trim(AB.C.CRight(dotfor, "#"))
		Dim ts1, ts2, ts3, sp, min
		If curtag = "" Then curtag = "a"
		If o_dict.Exists(ls&".index") Then
			If o_dict.Exists(ls&".min") Then min = Trim(o_dict(ls&".min"))
			If min="" Then min=0
			If IsNumeric(min) Then min = CLng(min)
			p = Trim(o_dict(ls&".index"))
			s = p : t = ""
			If Instr(p,"#")>0 Then
				s = Trim(AB.C.CLeft(p, "#"))
				t = Trim(AB.C.CRight(p, "#"))
			End If
			If IsNumeric(s) Then pa = abs(CLng(s))
			If IsNumeric(t) Then pb = abs(CLng(t))
			If pa<=0 Then pa = 0
			If pb<=0 Then pb = 0
			If pa>0 and pb=0 Then pb = pa
			If pb>0 and pa=0 Then pa = pb
		End If
		If o_dict.Exists(ls&".link-class") And Trim(o_dict(ls&".link-class"))<>"" Then a_class = Trim(o_dict(ls&".link-class"))
		If o_dict.Exists(ls&".link-id") And Trim(o_dict(ls&".link-id"))<>"" Then a_id = Trim(o_dict(ls&".link-id"))
		If o_dict.Exists(ls&".curr-class") And Trim(o_dict(ls&".curr-class"))<>"" Then b_class = Trim(o_dict(ls&".curr-class"))
		If o_dict.Exists(ls&".curr-id") And Trim(o_dict(ls&".curr-id"))<>"" Then b_id = Trim(o_dict(ls&".curr-id"))
		If o_dict.Exists(ls&".curr-wrap-tag") And Trim(o_dict(ls&".curr-wrap-tag"))<>"" Then curr_wrap_tag = Trim(o_dict(ls&".curr-wrap-tag"))
		If o_dict.Exists(ls&".curr-wrap-class") And Trim(o_dict(ls&".curr-wrap-class"))<>"" Then curr_wrap_class = Trim(o_dict(ls&".curr-wrap-class"))
		If o_dict.Exists(ls&".curr-wrap-id") And Trim(o_dict(ls&".curr-wrap-id"))<>"" Then curr_wrap_id = Trim(o_dict(ls&".curr-wrap-id"))
		If o_dict.Exists(ls&".link-wrap-tag") And Trim(o_dict(ls&".link-wrap-tag"))<>"" Then link_wrap_tag = Trim(o_dict(ls&".link-wrap-tag"))
		If o_dict.Exists(ls&".link-wrap-class") And Trim(o_dict(ls&".link-wrap-class"))<>"" Then link_wrap_class = Trim(o_dict(ls&".link-wrap-class"))
		If o_dict.Exists(ls&".link-wrap-id") And Trim(o_dict(ls&".link-wrap-id"))<>"" Then link_wrap_id = Trim(o_dict(ls&".link-wrap-id"))
		Dim i, k : temp = "" : k = 0
		If pMax>=1 Then
			If pa = 0 and pb = 0 Then
				k = 0
				For i = 1 To pMax '循环输出页码
					s = TmdClass(a_class) : t = TmdId(a_id)
					If(pNo = i) Then s = TmdClass(a_class&" "&b_class) : t = TmdId(a_id&" "&b_id)
					p1 = "<a href="""&LinkUrl(i)&""""&s&t&">"& GetLinkText(ltxt, i) &"<"&"/a>"
					If link_wrap_tag<>"" Then
						p = link_wrap_tag : s = "" : t = ""
						If link_wrap_class<>"" Then s=" class="""&link_wrap_class&""""
						If link_wrap_id<>"" Then t=" id="""&link_wrap_id&""""
						p1 = "<"&p&s&t&">" & p1 & "</"&p&">"
					End If
					p2 = "<"&curtag&s&t&">"& GetLinkText(ltxt, i) &"<"&"/"&curtag&">"
					If curr_wrap_tag<>"" Then
						p = curr_wrap_tag : s = "" : t = ""
						If curr_wrap_class<>"" Then s=" class="""&curr_wrap_class&""""
						If curr_wrap_id<>"" Then t=" id="""&curr_wrap_id&""""
						p2 = "<"&p&s&t&">" & p2 & "</"&p&">"
					End If
					p3 = GetLinkText(ltxt, i)
					p = p1
					If(pNo = i) Then
						If curtag = "a" Or curtag = "" Then p = p1 Else p = p2
						If curtag = "txt" Or curtag = "text" Then p = p3
					End If
					If(k = 0) Then
						temp = temp & p
					Else
						temp = temp & space & p
					End If
				Next
				k = k + 1
			Else
				Dim pn_min, pn_max : pn_min = pNo - pa : pn_max = pNo + pb
				If pn_min < 1 Then pn_min = 1
				If pn_max < pn_min+min-1 Then pn_max = pn_min+min-1
				If pn_max > pMax Then pn_max = pMax
				If pn_min > pn_max-min+1 Then pn_min = pn_max-min+1
				If pn_min<=0 Then pn_min=1
				If pn_min > 1 Then
					If dotval<>"" and dftag<>"" Then
						If dftag = "text" Then dftag = "txt"
						If Trim(space) = "" Then sp = " " Else sp = ""
						s = TmdClass(a_class) : t = TmdId(a_id)
						ts1 = "<a href="""&LinkUrl(1)&""""&s&t&">"& (GetLinkText(ltxt, 1)&" "&dotval) &"<"&"/a>"
						If link_wrap_tag<>"" Then
							p = link_wrap_tag : s = "" : t = ""
							If link_wrap_class<>"" Then s=" class="""&link_wrap_class&""""
							If link_wrap_id<>"" Then t=" id="""&link_wrap_id&""""
							ts1 = "<"&p&s&t&">" & ts1 & "</"&p&">"
						End If
						ts2 = "<a href="""&LinkUrl(1)&""""&s&t&">"& GetLinkText(ltxt, 1) &"<"&"/a>"
						If link_wrap_tag<>"" Then
							p = link_wrap_tag : s = "" : t = ""
							If link_wrap_class<>"" Then s=" class="""&link_wrap_class&""""
							If link_wrap_id<>"" Then t=" id="""&link_wrap_id&""""
							ts2 = "<"&p&s&t&">" & ts2 & "</"&p&">"
						End If
						ts3 = "<"&dftag&s&t&">"& (""&dotval&"") &"<"&"/"&dftag&">"
						ts4 = (""&dotval&"")
						If dftag="a" Then
							temp = temp & ts1
						ElseIf dftag="txt" Then
							temp = temp & ts2
						ElseIf dftag<>"" Then
							temp = temp & ts2 & sp & ts3
						End If
						If pn_min = 2 Then If dftag="a" Then temp = temp & space Else temp = temp & sp
						If pn_min > 2 Then
							If dftag="a" Or dftag<>"txt" Then If dfval="x" Or dfval="xy" Or dfval="" Then temp = temp & space
							If dftag="txt" Then If dfval="x" Or dfval="xy" Or dfval="" Then temp = temp & " " & ts4 & " "
						End If
					End If
				End If
				k = 0
				For i = pn_min To pn_max '循环输出页码
					s = TmdClass(a_class) : t = TmdId(a_id)
					If(pNo = i) Then s = TmdClass(a_class&" "&b_class) : t = TmdId(a_id&" "&b_id)
					p1 = "<a href="""&LinkUrl(i)&""""&s&t&">"& GetLinkText(ltxt, i) &"<"&"/a>"
					If link_wrap_tag<>"" Then
						p = link_wrap_tag : s = "" : t = ""
						If link_wrap_class<>"" Then s=" class="""&link_wrap_class&""""
						If link_wrap_id<>"" Then t=" id="""&link_wrap_id&""""
						p1 = "<"&p&s&t&">" & p1 & "</"&p&">"
					End If
					p2 = "<"&curtag&s&t&">"& GetLinkText(ltxt, i) &"<"&"/"&curtag&">"
					If curr_wrap_tag<>"" Then
						p = curr_wrap_tag : s = "" : t = ""
						If curr_wrap_class<>"" Then s=" class="""&curr_wrap_class&""""
						If curr_wrap_id<>"" Then t=" id="""&curr_wrap_id&""""
						p2 = "<"&p&s&t&">" & p2 & "</"&p&">"
					End If
					p3 = GetLinkText(ltxt, i)
					p = p1
					If(pNo = i) Then
						If curtag = "a" Or curtag = "" Then p = p1 Else p = p2
						If curtag = "txt" Or curtag = "text" Then p = p3
					End If
					If(k = 0) Then
						temp = temp & p
					Else
						temp = temp & space & p
					End If
					k = k + 1
				Next
				If pn_max < pMax Then
					If dotval<>"" and dftag<>"" Then
						If Trim(space) = "" Then sp = " " Else sp = ""
						s = TmdClass(a_class) : t = TmdId(a_id)
						ts1 = "<a href="""&LinkUrl(pMax)&""""&s&t&">"& (dotval&" "&GetLinkText(ltxt, pMax)) &"<"&"/a>"
						If link_wrap_tag<>"" Then
							p = link_wrap_tag : s = "" : t = ""
							If link_wrap_class<>"" Then s=" class="""&link_wrap_class&""""
							If link_wrap_id<>"" Then t=" id="""&link_wrap_id&""""
							ts1 = "<"&p&s&t&">" & ts1 & "</"&p&">"
						End If
						ts2 = "<a href="""&LinkUrl(pMax)&""""&s&t&">"& GetLinkText(ltxt, pMax) &"<"&"/a>"
						If link_wrap_tag<>"" Then
							p = link_wrap_tag : s = "" : t = ""
							If link_wrap_class<>"" Then s=" class="""&link_wrap_class&""""
							If link_wrap_id<>"" Then t=" id="""&link_wrap_id&""""
							ts2 = "<"&p&s&t&">" & ts2 & "</"&p&">"
						End If
						ts3 = "<"&dftag&s&t&">"& (""&dotval&"") &"<"&"/"&dftag&">"
						ts4 = (""&dotval&"")
						If pn_max = pMax-1 Then If dftag="a" Then temp = temp & space Else temp = temp & sp
						If pn_max < pMax-1 Then
							If dftag="a" Then If dfval="y" Or dfval="xy" Or dfval="" Then temp = temp & space
							If dftag="txt" Then If dfval="y" Or dfval="xy" Or dfval="" Then temp = temp & " " & ts4 & " "
							If dftag<>"a" and dftag<>"txt" Then If dfval="y" Or dfval="xy" Or dfval="" Then temp = temp & sp & ts3
						End If
						If dftag="a" Then
							temp = temp & ts1
						ElseIf dftag="txt" Then
							temp = temp & ts2
						ElseIf dftag<>"" Then
							temp = temp & sp & ts2
						End If
					End If
				End If
			End If
		End If
		If o_dict.Exists(ls&".wrap-tag") Then
			p = Trim(o_dict(ls&".wrap-tag")) : s = "" : t = ""
			If o_dict.Exists(ls&".wrap-class") And Trim(o_dict(ls&".wrap-class"))<>"" Then s=" class="""&Trim(o_dict(ls&".wrap-class"))&""""
			If o_dict.Exists(ls&".wrap-id") And Trim(o_dict(ls&".wrap-id"))<>"" Then t=" id="""&Trim(o_dict(ls&".wrap-id"))&""""
			If p <> "" Then temp = "<"&p&s&t&">" & temp & "</"&p&">"
		End If
		If Trim(o_dict(ls&".display"))="auto" And Me.PageCount()<=1 Then temp = ""
		GetList = temp
		'If o_cache("ab_page:list").Ready Then : o_cache("ab_page:list").Del : o_cache("ab_page:dict").Del : End If
		o_cache("ab_page:list").Del : o_cache("ab_page:dict").Del
		o_cache("ab_page:list") = temp : o_cache("ab_page:list").SaveApp
		o_cache("ab_page:dict") = o_dict : o_cache("ab_page:dict").SaveApp
	End Function

	Private Function dictHas(ByVal o, ByVal p)
		Dim tmp : tmp = False
		If AB.C.IsDict(o) Then
			If o.Exists(p) Then
				If Trim(o(p))<>"" Then
					tmp = True
				End If
			End If
		End If
		dictHas = tmp
	End Function

	Private Function GetSelect()
		Dim tmp, opts, ls, name, text, s, t : s = "" : t = "" : ls = "select"
		Dim pNo, pMax : pNo = Me.PageNo : pMax = Me.PageCount
		Dim pn_min, pn_max : pn_min = 1 : pn_max = pMax
		Dim st, sp, seled
		If dictHas(o_dict, ls&".name") Then name = " name="""& Trim(o_dict(ls&".name")) &""""
		If dictHas(o_dict, ls&".text") Then text = Trim(o_dict(ls&".text"))
		If dictHas(o_dict, ls&".class") Then s = TmdClass(o_dict(ls&".class"))
		If dictHas(o_dict, ls&".id") Then t = TmdId(o_dict(ls&".id"))
		tmp = "<select"& name & s & t &" onchange=""javascript:location='?page='+this.options[this.selectedIndex].value;"">"
		opts = ""
		For i = pn_min To pn_max
			If i = pNo Then
				If dictHas(o_dict, ls&".curr-class") Then st = TmdClass(o_dict(ls&".curr-class"))
				If dictHas(o_dict, ls&".curr-id") Then sp = TmdId(o_dict(ls&".curr-id"))
				seled = " selected"
			Else
				st = "" : sp = "" : seled = ""
			End If
			value = text
			value = AB.C.RP(value,Array("\$","$n",Chr(25)),Array("$",i,Chr(25)))
			If value = "" Then value = "第"& i &"页"
			opts = opts & "<option value="""& i &""""& st & sp & seled &">"& value &"</option>"
		Next
		tmp = tmp & opts
		tmp = tmp & "</select>"
		GetSelect = tmp
	End Function

	Private Sub ProcessStyle(ByVal s)
		If IsNull(s) Or Trim(s) = "" Then Exit Sub
		s = AB.C.RP(s, Array("\{","\}","\;"), Array(Chr(28),Chr(29),Chr(30)))
		Dim Matches, Match, Mt, rule, tag, txt, a, item, key, value, v
		rule = "(first|prev|next|last|list|select)\{([^\{}]*)\}"
		Set Matches = AB.C.RegMatch(s, rule)
		For Each Match In Matches
			tag = Trim(LCase(Match.SubMatches(0)))
			txt = Trim(Match.SubMatches(1))
			a = Split(txt,";")
			AB.Use "A"
			If AB.A.Len(a)>0 Then
				For Each item In a
					If InStr(item,":")>0 Then
						key = Trim(AB.C.CLeft(item, ":"))
						value = Trim(AB.C.CRight(item, ":"))
						If InStr(value,"|")>0 Then
							Dim r1, r2
							r1 = "^((['""])(.*?)\2\s*\|.*)$"
							r2 = "((['""])(.*?)\2)"
							r3 = "^(\s*\|\s*.+)"
							If value="|" Then
								value = "|"
							ElseIf AB.C.RegTest(value, r3) Then
								value = ""
							ElseIf AB.C.RegTest(value, r1) Then
								Set Mt = AB.C.RegMatch(value, r1)
								If Mt.Count>0 Then value = Mt(0).SubMatches(2)
							ElseIf AB.C.RegTest(value, r2) Then
								Set Mt = AB.C.RegMatch(value, r2)
								If Mt.Count>0 Then value = Mt(0).SubMatches(2)
							Else
								value = Trim(AB.C.CLeft(value, "|"))
							End If
							Set Mt = Nothing
						End If
						value = AB.C.RegReplace(value, "^((['""])(.*?)\2)$", "$3")
						If tag = "list" Then '处理：list{}
							v = Trim(value)
							v = AB.C.RP(v, Array(Chr(28),Chr(29),Chr(30)), Array("{","}",";"))
							If key = "display" and (v="show" Or v="hide" Or v="auto") Then o_dict(tag&".display") = FK(v)
							If key = "wrap-tag" and v <> "" Then o_dict(tag&".wrap-tag") = FK(v)
							If key = "wrap-class" and v <> "" Then o_dict(tag&".wrap-class") = FK(v)
							If key = "wrap-id" and v <> "" Then o_dict(tag&".wrap-id") = FK(v)
							If key = "curr-tag" and v <> "" Then o_dict(tag&".curr-tag") = FK(v)
							If key = "curr-class" and v <> "" Then o_dict(tag&".curr-class") = FK(v)
							If key = "curr-id" and v <> "" Then o_dict(tag&".curr-id") = FK(v)
							If key = "link-text" and v <> "" Then o_dict(tag&".link-text") = FK(v)
							If key = "link-class" and v <> "" Then o_dict(tag&".link-class") = FK(v)
							If key = "link-id" and v <> "" Then o_dict(tag&".link-id") = FK(v)
							If key = "link-space" Then o_dict(tag&".link-space") = value
							If key = "dot-val" and v <> "" Then o_dict(tag&".dot-val") = value
							If key = "dot-for" and v <> "" Then o_dict(tag&".dot-for") = FK(v)
							If key = "index" and v <> "" Then o_dict(tag&".index") = FK(v)
							If key = "min" and v <> "" Then o_dict(tag&".min") = FK(v)
							If key = "curr-wrap-tag" and v <> "" Then o_dict(tag&".curr-wrap-tag") = FK(v)
							If key = "curr-wrap-class" and v <> "" Then o_dict(tag&".curr-wrap-class") = FK(v)
							If key = "curr-wrap-id" and v <> "" Then o_dict(tag&".curr-wrap-id") = FK(v)
							If key = "link-wrap-tag" and v <> "" Then o_dict(tag&".link-wrap-tag") = FK(v)
							If key = "link-wrap-class" and v <> "" Then o_dict(tag&".link-wrap-class") = FK(v)
							If key = "link-wrap-id" and v <> "" Then o_dict(tag&".link-wrap-id") = FK(v)
							'--空值默认处理:
							If key = "display" and (v <> "show" And v <> "hide" And v <> "auto") Then o_dict(tag&".display") = "show"
							If key = "link-text" and v = "" Then o_dict(tag&".link-text") = "$n"
							If key = "curr-tag" and v = "" Then o_dict(tag&".curr-tag") = "a"
							If key = "curr-class" and v = "" Then o_dict(tag&".curr-class") = "current"
						ElseIf tag = "select" Then '处理：select{}
							v = Trim(value)
							v = AB.C.RP(v, Array(Chr(28),Chr(29),Chr(30)), Array("{","}",";"))
							If key = "display" and v <> "" Then o_dict(tag&".display") = FK(v)
							If key = "name" and v <> "" Then o_dict(tag&".name") = FK(v)
							If key = "text" and v <> "" Then o_dict(tag&".text") = FK(v)
							If key = "class" and v <> "" Then o_dict(tag&".class") = FK(v)
							If key = "id" and v <> "" Then o_dict(tag&".id") = FK(v)
							If key = "curr-class" and v <> "" Then o_dict(tag&".curr-class") = FK(v)
							If key = "curr-id" and v <> "" Then o_dict(tag&".curr-id") = FK(v)
						Else
							v = Trim(value)
							v = AB.C.RP(v, Array(Chr(28),Chr(29),Chr(30)), Array("{","}",";"))
							If key = "display" and (v="show" Or v="hide" Or v="auto") Then o_dict(tag&".display") = FK(v)
							If key = "dis-class" and v <> "" Then o_dict(tag&".dis-class") = FK(v)
							If key = "dis-id" and v <> "" Then o_dict(tag&".dis-id") = FK(v)
							If key = "class" and v <> "" Then o_dict(tag&".class") = FK(v)
							If key = "id" and v <> "" Then o_dict(tag&".id") = FK(v)
							If key = "dis-tag" and v <> "" Then o_dict(tag&".dis-tag") = FK(v)
							If key = "dis-href" Then
								If Lcase(v)="null" Or v="" Or Lcase(v)="empty" Then o_dict(tag&".dis-href") = "" Else o_dict(tag&".dis-href") = FK(v)
							End If
							If key = "wrap-tag" and v <> "" Then o_dict(tag&".wrap-tag") = FK(v)
							If key = "wrap-class" and v <> "" Then o_dict(tag&".wrap-class") = FK(v)
							If key = "wrap-id" and v <> "" Then o_dict(tag&".wrap-id") = FK(v)
							If key = "text" and v <> "" Then o_dict(tag&".text") = v
							'--空值默认处理:
							If key = "display" and (v <> "show" And v <> "hide" And v <> "auto") Then o_dict(tag&".display") = "show"
							If key = "class" and v = "" Then o_dict(tag&".class") = tag
							If key = "id" and v = "" Then o_dict(tag&".id") = tag
							If key = "dis-tag" and v = "" Then o_dict(tag&".dis-tag") = "a"
							If key = "wrap-tag" and v = "" Then o_dict(tag&".wrap-tag") = ""
							If key = "text" and v = "" Then
								If tag = "first" Then o_dict(tag&".text") = "首 页"
								If tag = "prev" Then o_dict(tag&".text") = "上一页"
								If tag = "next" Then o_dict(tag&".text") = "下一页"
								If tag = "last" Then o_dict(tag&".text") = "尾 页"
							End If
						End If
					End If
				Next
			End If
		Next
		Set Matches = Nothing
	End Sub

End Class
%>