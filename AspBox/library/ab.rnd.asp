<%
'######################################################################
'## ab.rnd.asp
'## -------------------------------------------------------------------
'## Feature     :   AspBox Rand Class
'## Version     :   v1.0
'## Author      :   Lajox(lajox@19www.com)
'## Update Date :   2012/12/18 15:04
'## Description :   AspBox随机函数模块
'######################################################################

Class Cls_AB_Rnd

	Private Sub Class_Initialize()

	End Sub

	Private Sub Class_Terminate()

	End Sub

	'*******************************************
	' 函数名: Rand
	' 参  数: str as the input string
	' 作  用: Generate a Random integer
	'*******************************************
	Public Function Rand(Byval min, Byval max)
		Randomize
		Rand = Int((max - min + 1) * Rnd + min)
	End Function

	'*******************************************
	' 函数名: strRand
	' 作用: Generate a specific length Random string
	'*******************************************
	Public Function strRand(Byval intLen)
		Dim PassMask,sLength,i,temp
		PassMask = "abcdefghijklmnopqrstuvwxyz1234567890"
		sLength = len(PassMask)
		For i=1 to intLen
			Randomize
			temp = temp & Mid(PassMask,Round((Rnd*(sLength-1))+1),1)
		Next
		strRand = temp
	End Function

	'生成几位随机数
	Public Function GetRand(n)
		Dim num
		Dim rndnum
		Randomize
		Do While Len(rndnum)<n
			num = CStr(Chr((57 -48) * Rnd + 48))
			rndnum = rndnum & num
		Loop
		GetRand = rndnum
	End Function

	'生成随机密码(intLen为新密码长度; PassMask为掩码,可为空)
	Public Function GenPassword(Byval intLen, Byval PassMask)
		Dim temp:temp=""
		Dim iCnt, PosTemp
		Randomize
		If PassMask="" Then PassMask = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz"
		For iCnt = 1 To intLen
			PosTemp = Fix(Rnd(1) * (Len(PassMask))) + 1
			temp = temp & Mid(PassMask, PosTemp, 1)
		Next
		GenPassword = temp
	End Function

	'随机生成汉字(intLen为随机汉字数)
	Public Function RandChinese(Byval intLen)
		Dim i,temp,a,b: temp=""
		Dim HS,HE,LS,LE,Max
		HS = 177 : HE = 247 : LS = 161 : LE = 254 : Max = 65536
		Randomize
		AB.Use "Char"
		For i = 1 To intLen
			a=AB.Char.C10To2(Int((HE - HS) * Rnd()) + HS)
			b=AB.Char.C10To2(Int((LE - LS) * Rnd()) + LS)
			temp = temp & Chr(AB.Char.C2To10(a & b) - Max)
		Next
		RandChinese = temp
	End Function

	'生成序列号
	Public Function GenSerialString()
		Dim temp:temp=""
		temp = temp & Year(Now()) & Right("00"&Month(now),2) & Right("00"&Day(now),2) & Right("00"&Hour(now),2) & Right("00"&Minute(now),2) & Right("00"&Second(now),2)
		temp = temp & GenPassword(6, "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ")
		GenSerialString = temp
	End Function

	'系统分配随机密码
	Public Function CreateRndPass()
		Dim temp:temp=""
		Dim Ran,i,LengthNum
		LengthNum=16
		For i=1 To LengthNum
			Randomize
			Ran = CInt(Rnd * 2)
			Randomize
			If Ran = 0 Then
				Ran = CInt(Rnd * 25) + 97
				temp = temp & UCase(Chr(Ran))
			ElseIf Ran = 1 Then
				Ran = CInt(Rnd * 9)
				temp = temp & Ran
			ElseIf Ran = 2 Then
				Ran = CInt(Rnd * 25) + 97
				temp = temp & Chr(Ran)
			End If
		Next
		CreateRndPass = temp
	End Function

	'随机文件名
	Public Function GetRndFileName(Byval ext)
		Dim RanNum,temp
		Randomize
		RanNum = Int(90000*rnd)+10000
	 	temp = Year(now) & Right("00"&Month(now),2) & Right("00"&Day(now),2) & Right("00"&Hour(now),2) & Right("00"&Minute(now),2) & Right("00"&Second(now),2)
		temp = temp & RanNum
	 	If ext<>"" then temp = temp & "." & ext
		GetRndFileName = temp
	End Function

	'随机订单号
	Public Function RndOrderNO(Byval n)
		dim i, j, str, temp
		For j=1 To n
			Randomize Timer
			str = str & Chr(Int((57 - 48 + 1) * Rnd + 48))
		Next
	 	temp = Year(now) & Right("00"&Month(now),2) & Right("00"&Day(now),2) & Right("00"&Hour(now),2) & Right("00"&Minute(now),2) & Right("00"&Second(now),2)
		str = temp & str
		RndOrderNO = str
	End function

	'------------------------------------------------------------------------------------------
	'# AB.Rnd.MakeRndPass(s)
	'# @return: string
	'# @dowhat: 生成随机密码
	'--DESC------------------------------------------------------------------------------------
	'# @param passlen	: [string] (字符串) 要生成的密码长度
	'# @param passtype	: [string] (字符串) 要生成的密码类型
	'# ==类型解释：
	'#     passfull       		（缺省字符：所有字母及数字）	简写：full, f, ""
	'#     passnumber     		（纯数字）						简写：num, n
	'#     passspecial    		（非常用字符）					简写：s
	'#     passCharNumber 		（所有字母及数字）				简写：cn, nc
	'#     passUpperCharNumber 	（大写字母数字）				简写：ucn, un, nu
	'#     passLowerCharNumber 	（小写字母数字）				简写：lcn, ln, nl
	'#     passChar       		（所有大小写字母）				简写：c
	'#     passUpperChar  		（所有大写字母）				简写：u
	'#     passLowerChar  		（所有小写字母）				简写：l
	'--DEMO------------------------------------------------------------------------------------
	'# ab.c.print AB.Rnd.MakeRndPass(4, "un") '=> YVN2
	'------------------------------------------------------------------------------------------

	Public Function MakeRndPass(ByVal passlen, ByVal passtype)
		makeRndPass=""
		dim passFull,passNumber,passSpecial,passCharNumber,passChar,pass
		dim passUpperCharNumber,passLowerCharNumber,passUpperChar,passLowerChar,ii,jj
		passFull = ""
		passNumber = "1234567890"
		passSpecial = "!#$%&""'()*+,.-_/:;<=>?@[\]^`{|}~%"
		passCharNumber = "abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ"
		passUpperCharNumber = "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ"
		passLowerCharNumber = "abcdefghijklmnopqrstuvwxyz1234567890"
		passChar = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
		passUpperChar = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
		passLowerChar = "abcdefghijklmnopqrstuvwxyz"
		select case lcase(trim(passType))
		case "passfull","full","f",""
			pass = passFull
		case "passspecial","special","sp","s"
			pass = passSpecial
		case "passnumber","number","num","n"
			pass = passNumber
		case "passcharnumber","charnumber","charnum","cn","nc"
			pass = passCharNumber
		case "passchar","char","c"
			pass = passChar
		case "passupperchar","upperchar","upper","uc","u"
			pass = passUpperChar
		case "passlowerchar","lowerchar","lower","lc","l"
			pass = passLowerChar
		case "passuppercharnumber","uppercharnumber","uppercharnum","uppernumber","uppernum","ucn","un","nu"
			pass = passUpperCharNumber
		case "passlowercharnumber","lowercharnumber","lowercharnum","lowernumber","lowernum","lcn","ln","nl"
			pass = passLowerCharNumber
		case else
			pass = passCharNumber
		end select
		If trim(pass)="" Then pass = passCharNumber
		for ii=1 to cint(passlen)
			randomize
			jj = int(rnd()*len(pass)+1)
			makeRndPass = cstr(makeRndPass) & mid(pass,jj,1)
		next
	End Function

	'------------------------------------------------------------------------------------------
	'# AB.Rnd.RndKey(s)
	'# @return: string
	'# @dowhat: 生成随机字符串
	'--DESC------------------------------------------------------------------------------------
	'# @param digits : [integer] (整数) 要生成的字符串位数
	'--DEMO------------------------------------------------------------------------------------
	'# ab.c.print AB.Rnd.RndKey(5) '=> YM5sH
	'------------------------------------------------------------------------------------------

	Function RndKey(Byval digits)
		Dim char_array(80)
		Dim i,num,output
		For i = 0 To 9 : char_array(i) = CStr(i) : Next '初始化数字
		For i = 10 To 35 : char_array(i) = Chr(i + 55) : Next '初始化大写字母
		For i = 36 To 61 : char_array(i) = Chr(i + 61) : Next '初始化小写字母
		Randomize
		Do while Len(output) < digits
			num = char_array(Int((62 - 0 + 1) * Rnd + 0))
			output = output & num
		Loop
		RndKey = output
	End Function

End Class
%>